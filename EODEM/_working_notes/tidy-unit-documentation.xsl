<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 	
	xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:lido="http://www.lido-schema.org" xmlns:profile="http://www.lido-schema.org/lidoProfile/" exclude-result-prefixes="tei lido profile">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:variable name="rupert-doc" select="document('EODEM_LIDO_profile_documentation__V0_07_02.xml')"/>
	<xsl:template match="*" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="*" priority="-1" mode="tei">
		<xsl:element name="{concat('tei:', local-name())}">
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates mode="tei"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*" priority="-1">
		<xsl:copy-of select="."/>
	</xsl:template>
	<xsl:template match="profile:schemaDoc">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<tei:TEI>
				<xsl:apply-templates select="$rupert-doc/tei:TEI/tei:teiHeader" mode="tei"/>
				<tei:text xml:lang="en">
					<xsl:apply-templates select="$rupert-doc/tei:TEI/tei:text/tei:front" mode="tei"/>
					<tei:body>
						<xsl:apply-templates/>
					</tei:body>
					<xsl:apply-templates select="$rupert-doc/tei:TEI/tei:text/tei:back" mode="tei"/>
				</tei:text>
			</tei:TEI>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:div[@type='ap_unit'][@xml:id]">
		<xsl:variable name="this-id" select="@xml:id"/>
		<xsl:variable name="rupert-doc-unit" select="$rupert-doc//tei:div[@xml:id=$this-id]"/>
		<xsl:if test="count($rupert-doc-unit)=0"><xsl:message>Corresponding Rupert unit not found: <xsl:value-of select="$this-id"/></xsl:message></xsl:if>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="profile:head|tei:head" mode="tei"/>
			<xsl:choose>
				<xsl:when test="tei:div[@type='ap_definition']">
					<xsl:apply-templates select="tei:div[@type='ap_definition']"/>
				</xsl:when>
				<xsl:when test="$rupert-doc-unit/tei:div[@type='ap_definition']">
					<xsl:apply-templates select="$rupert-doc-unit/tei:div[@type='ap_definition']" mode="tei"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:message>No definition given for <xsl:value-of select="$this-id"/></xsl:message>
					<tei:div type="ap_definition">
						<tei:p>**to add**</tei:p>
					</tei:div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test=".//tei:div[@type='ap_examples']">
					<xsl:apply-templates select=".//tei:div[@type='ap_examples']"/>
				</xsl:when>
				<xsl:when test="$rupert-doc-unit//tei:div[@type='ap_examples_export']">
					<tei:div type="ap_examples">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='ap_examples_export']/*" mode="tei"/>
					</tei:div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:message>No examples given for <xsl:value-of select="$this-id"/></xsl:message>
					<tei:div type="ap_examples">
						<tei:list><tei:item>**to add**</tei:item></tei:list>
					</tei:div>
				</xsl:otherwise>
			</xsl:choose>
			<tei:div type="ap_how_to_record">
				<tei:div type="ap_references">
					<xsl:if test="$rupert-doc-unit//tei:div[@type='ap_references']">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='ap_references']/*" mode="tei"/>
					</xsl:if>
				</tei:div>
				<tei:div type="ap_authority">
					<xsl:if test="$rupert-doc-unit//tei:div[@type='authority']">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='authority']/*" mode="tei"/>
					</xsl:if>					
				</tei:div>
				<tei:div type="ap_cardinality">
					<xsl:if test="$rupert-doc-unit//tei:div[@type='cardinality']">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='cardinality']/*" mode="tei"/>
					</xsl:if>					
				</tei:div>
				<tei:div type="ap_content_type">
					<xsl:if test="$rupert-doc-unit//tei:div[@type='content_type']">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='content_type']/*" mode="tei"/>
					</xsl:if>					
				</tei:div>
				<tei:div type="ap_divergence_from_lido">
					<xsl:if test="$rupert-doc-unit//tei:div[@type='divergence_from_lido']">
						<xsl:apply-templates select="$rupert-doc-unit//tei:div[@type='divergence_from_lido']/*" mode="tei"/>
					</xsl:if>					
				</tei:div>
				<tei:div type="ap_xpath_interchange">
					<tei:p></tei:p>
				</tei:div>
				<tei:div type="ap_xpath_object">
					<tei:p></tei:p>
				</tei:div>
			</tei:div>
			<tei:div type="ap_export">
				<xsl:apply-templates select="tei:div[@type='ap_context']"/>
				<xsl:apply-templates select="tei:div[@type='ap_structure']"/>
			</tei:div>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>