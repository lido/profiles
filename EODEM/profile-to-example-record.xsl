<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:profile="http://www.lido-schema.org/lidoProfile/" 
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:lido="http://www.lido-schema.org"
	xmlns:eodem="http://www.eodem-schema.org" 
	exclude-result-prefixes="tei profile">
	<xsl:output method="xml" indent="yes" encoding="UTF-8" cdata-section-elements="lido:descriptiveNoteValue"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//tei:div[@type='ap_unit'][1]" mode="build">
			<xsl:with-param name="level" select="0"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="tei:div[@type='ap_unit']" mode="build">
		<xsl:param name="level"/>
		<xsl:variable name="eodem-id" select="@xml:id"/>
		<xsl:choose>
			<xsl:when test="tei:div[@type='ap_export']/tei:div[@type='ap_structure']">
				<xsl:apply-templates select="tei:div[@type='ap_export']/tei:div[@type='ap_structure']/*" mode="build">
					<xsl:with-param name="level" select="$level"/>
					<xsl:with-param name="eodem-id" select="$eodem-id"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="tei:div[@type='ap_export']/tei:div[@type='ap_attribute']">
				<xsl:apply-templates select="tei:div[@type='ap_export']/tei:div[@type='ap_attribute']/tei:p/text()" mode="build-attribute"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>Structure for <xsl:value-of select="$eodem-id"/> not specified</xsl:comment>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*" mode="build" priority="-1">
		<xsl:param name="level"/>
		<xsl:param name="eodem-id"/>
		<xsl:copy>
			<xsl:if test="string($eodem-id)">
				<xsl:attribute name="eodem:unit"><xsl:value-of select="$eodem-id"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@*" mode="build"/>
			<xsl:apply-templates mode="build">
				<xsl:with-param name="level" select="$level"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="text()" mode="build">
		<xsl:choose>
			<xsl:when test=".='#'">
				<xsl:apply-templates select="ancestor::tei:div[@type='ap_unit']" mode="value"/>
			</xsl:when>
			<xsl:when test="starts-with(., '#')">
				<xsl:apply-templates select="id(substring-after(., '#'))" mode="value"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="text()" mode="build-attribute">
		<xsl:attribute name="{.}"></xsl:attribute>
	</xsl:template>
	<xsl:template match="@*" mode="build" priority="-1">
		<xsl:attribute name="{name()}">
			<xsl:choose>
				<xsl:when test="starts-with(., '#')">
					<xsl:apply-templates select="id(substring-after(., '#'))" mode="value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:if test="starts-with(., '#')">
			<xsl:attribute name="eodem:unit2"><xsl:value-of select="substring-after(., '#')"/></xsl:attribute>
		</xsl:if>
		<!--xsl:copy/-->
	</xsl:template>
	<xsl:template match="tei:div" mode="value">
		<xsl:value-of select="tei:div[@type='ap_examples']/tei:list/tei:item[1]"/>		
	</xsl:template>
	<xsl:template match="tei:ref" mode="build">
		<xsl:param name="level"/>
		<xsl:variable name="target-id" select="substring-after(@target, '#')"/>
		<xsl:variable name="lookup" select="id($target-id)"/>
		<!--xsl:message>Looking up <xsl:value-of select="substring-after(@target, '#')"/>; level is <xsl:value-of select="$level"/></xsl:message-->
		<xsl:if test="$level &lt; 100">
			<xsl:choose>
				<xsl:when test="$lookup">
					<!--xsl:message>Lookup of <xsl:value-of select="@target"/> succeeded</xsl:message-->
					<xsl:apply-templates select="$lookup/tei:div[@type='ap_export']/tei:div[@type='ap_structure']/*" mode="build">
						<xsl:with-param name="level" select="$level+1"/>
						<xsl:with-param name="eodem-id" select="$target-id"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:comment>Section for <xsl:value-of select="@target"/> not yet provided</xsl:comment>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>