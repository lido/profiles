<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:eodem="http://www.eodem-schema.org" xmlns:lido="http://www.lido-schema.org" xmlns:tei="http://www.tei-c.org/ns/1.0" >
	<xsl:output method="html" indent="yes"/>
	<xsl:param name="docs" select="'lido-v1.1-profile-EODEM-v1.0.xml'"/>
	<xsl:param name="htmldoc" select="'lido-v1.1-profile-EODEM-v1.0.html'"/>
	<xsl:param name="indent" select="8"/>
	<xsl:variable name="quot">&quot;</xsl:variable>
	<xsl:template match="/">
		<html>
			<head>
				<title>EODEM document structure</title>
				<style>
					<![CDATA[
						td {border-top: 1px solid black; vertical-align: top;}
					]]>
				</style>
			</head>
			<body>
				<h2>EODEM document structure</h2>
				<table>
					<tr>
						<th>LIDO element</th>
						<th>Attribute(s)</th>
						<th>Example value</th>
						<th>EODEM unit(s) of information</th>
					</tr>
					<xsl:apply-templates/>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="*">
		<tr>
			<td><span style="padding-left: {count(ancestor::*)*$indent}px;"><xsl:value-of select="name()"/></span></td>
			<td><xsl:apply-templates select="@*[name()!='eodem:unit' and name()!='eodem:unit2']"/></td>
			<td><xsl:apply-templates select="text()"/></td>
			<td><xsl:apply-templates select="@eodem:unit|@eodem:unit2"/></td>
		</tr>
		<xsl:apply-templates select="*"/>
	</xsl:template>
	<xsl:template match="@eodem:unit|@eodem:unit2" priority="2">
		<xsl:if test="name()='eodem:unit2' and string(../@eodem:unit)"><br/></xsl:if>
		<xsl:variable name="eodem_ID">
			<xsl:choose>
				<xsl:when test="contains(., '-&gt;')">
					<xsl:value-of select="substring-before(., '-&gt;')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="document($docs)/id($eodem_ID)">
				<b><xsl:apply-templates select="document($docs)/id($eodem_ID)"/></b>
			</xsl:when>
			<xsl:otherwise>[<xsl:value-of select="$eodem_ID"/>]</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	<xsl:template match="@*" priority="-1">
		<xsl:choose>
			<xsl:when test="starts-with(., '#')">
				<xsl:variable name="eodem_ID" select="substring-after(., '#')"/>
				<xsl:value-of select="concat(name(), '=', $quot)"/>
				<xsl:choose>
					<xsl:when test="document($docs)/id($eodem_ID)">
						<xsl:apply-templates select="document($docs)/id($eodem_ID)"/>
					</xsl:when>
					<xsl:otherwise>[<xsl:value-of select="$eodem_ID"/>]</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$quot"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(name(), '=', $quot, ., $quot)"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<br/>
	</xsl:template>
	<xsl:template match="text()">
		<xsl:call-template name="output-extract"/>	
	</xsl:template>
	<xsl:template name="output-extract">
		<xsl:param name="length" select="100"/>
		<xsl:choose>
			<xsl:when test="string-length(.) &lt;= $length">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring(., 1, $length)"/>
				<xsl:value-of select="substring-before(substring(., $length+1), ' ')"/>
				<xsl:text> ...</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tei:div">
		<a href="{concat($htmldoc, '#', @xml:id)}" target="_blank"><xsl:value-of select="tei:head"/></a>
	</xsl:template>
</xsl:stylesheet>