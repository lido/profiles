<?xml version="1.0" encoding="utf-8"?>
<!--  Create LIDO profile XSD. Version 1.0, Richard Light 18 May 2023. -->
<!--  Currently replace, change and delete have been implemented.	
		Also implemented support for the profile's import element. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:profile="http://www.lido-schema.org/lidoProfile/"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:lido="http://www.lido-schema.org"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:sch="http://purl.oclc.org/dsdl/schematron"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="profile xsi tei xs sch">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:variable name="schema-spec" select="/profile:lidoProfile/profile:schemaSpec"/>
	<xsl:variable name="profile-elements" select="/profile:lidoProfile/profile:schemaSpec/*"/>
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="profile:lidoProfile">
		<xsl:apply-templates select="profile:schemaSpec"/>
	</xsl:template>
	<xsl:template match="profile:schemaSpec">
		<xsl:variable name="source" select="document(@base)"/>
		<xsl:apply-templates select="$source" mode="update"/>
	</xsl:template>
	
	<xsl:template match="*" mode="copy" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="copy"/>
			<xsl:apply-templates mode="copy"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*" mode="copy" priority="-1">
		<xsl:copy-of select="."/>
	</xsl:template>
	<xsl:template match="@mode" mode="copy"/>
	<!--xsl:template match="@type" mode="copy"/--> <!-- type attribute is required in the generated Schema -->
	<xsl:template match="@elttype" mode="copy"/>
	<xsl:template match="@xml:id" mode="copy">
		<xsl:attribute name="id">
			<xsl:choose>
				<xsl:when test="contains(., ':')">
					<xsl:value-of select="substring-after(., ':')"/>
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>
	
	<!-- Updating the schema with profile elements: -->
	<xsl:template match="*" mode="update" priority="-1">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="update"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="xs:schema" mode="update">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="xs:annotation" mode="update"/>
			<xsl:apply-templates select="xs:import" mode="update"/>
			<xsl:apply-templates select="$schema-spec/profile:import" mode="update"/>
			<xsl:apply-templates select="*[not(self::xs:annotation or self::xs:import)]" mode="update"/>
			<xsl:apply-templates select="$profile-elements[@mode='add']" mode="add"/>
		</xsl:copy>
	</xsl:template>	
	<xsl:template match="profile:import" mode="update">
		<xs:import>
			<xsl:copy-of select="@*"/>
		</xs:import>
	</xsl:template>
	<xsl:template match="xs:element[string(@id)]" mode="update">
		<xsl:variable name="id" select="@id"/>
		<xsl:variable name="profile-override" select="$profile-elements[@xml:id=$id or @xml:id=concat('lido:', $id)]"/>
		<xsl:choose>
			<xsl:when test="count($profile-override)&gt;0">
				<xsl:message>Profile override for <xsl:value-of select="$id"/> found.</xsl:message>
				<xsl:choose>
					<xsl:when test="$profile-override[@mode='change']">
						<xsl:apply-templates select="." mode="change">
							<xsl:with-param name="profile" select="$profile-override"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:when test="$profile-override[@mode='replace']">
						<xsl:copy>
							<xsl:apply-templates select="$profile-override/@*" mode="copy"/>
							<xsl:apply-templates select="$profile-override/*" mode="copy"/>
						</xsl:copy>
					</xsl:when>
					<xsl:when test="$profile-override[@mode='delete']"></xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="@*" mode="copy"/>
							<xsl:apply-templates mode="update"/>
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*" mode="copy"/>
					<xsl:apply-templates mode="update"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="xs:element[string(@ref)]" mode="update">
		<xsl:variable name="ref" select="@ref"/>
		<xsl:variable name="profile-override" select="$profile-elements[@xml:id=$ref]"/>
		<xsl:choose>
			<xsl:when test="count($profile-override)&gt;0">
				<!--xsl:message>Profile override for <xsl:value-of select="$id"/> found.</xsl:message-->
				<xsl:choose>
					<!--xsl:when test="$profile-override[@mode='replace']">
						<xsl:apply-templates select="$profile-override" mode="copy"/>
					</xsl:when-->
					<xsl:when test="$profile-override[@mode='delete']"></xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="@*" mode="copy"/>
							<xsl:apply-templates mode="update"/>
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*" mode="copy"/>
					<xsl:apply-templates mode="update"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*" mode="update" priority="-1">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<!-- Change mode:-->
	<xsl:template match="*" mode="change">
		<xsl:param name="profile"/>
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="change">
				<xsl:with-param name="profile" select="$profile"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="inner-change">
				<xsl:with-param name="profile" select="$profile"/>
			</xsl:apply-templates>
			<!--xsl:apply-templates select="$profile/*" mode="add-if-new">
				<xsl:with-param name="existing-defn" select="."/>
			</xsl:apply-templates-->
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*" mode="change">
		<xsl:param name="profile"/>
		<xsl:variable name="att-name" select="name()"/>
		<xsl:choose>
			<xsl:when test="string($profile/@*[name()=$att-name])">
				<xsl:copy-of select="$profile/@*[name()=$att-name]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*" mode="inner-change">
		<xsl:param name="profile"/>
		<xsl:variable name="elt-name" select="local-name()"/>
		<!--xsl:message>Looking to match <xsl:value-of select="$elt-name"/> in elementSpec</xsl:message-->
		<xsl:choose>
			<xsl:when test="count($profile/*[local-name()=$elt-name]) &gt; 0">
				<xsl:message>Inner change for <xsl:value-of select="$elt-name"/> found</xsl:message>
				<xsl:copy-of select="$profile/*[local-name()=$elt-name]"/>
			</xsl:when>
			<xsl:otherwise>
				<!--xsl:comment> <xsl:value-of select="$elt-name"/> copied </xsl:comment-->
				<xsl:copy>
					<xsl:apply-templates select="@*" mode="change">
						<xsl:with-param name="profile" select="$profile"/>
					</xsl:apply-templates>
					<xsl:apply-templates mode="inner-change">
						<xsl:with-param name="profile" select="$profile"/>
					</xsl:apply-templates>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*" mode="add-if-new">
		<xsl:param name="existing-defn"/>
		<xsl:variable name="elt-name" select="name()"/>
		<xsl:if test="not($existing-defn/*[name()=$elt-name])">
			<xsl:apply-templates select="." mode="copy"/>
		</xsl:if>
	</xsl:template>
	
	<!-- Add mode: removed 18.5.2023-->
	<xsl:template match="*" mode="add">
		<!-- !! check that Id doesn't exist and that @type is an allowed one: -->
		<!--xsl:element name="{concat('xs:', @elttype)}">
			<xsl:apply-templates select="@*" mode="copy"/>
			<xsl:apply-templates mode="copy"/>
		</xsl:element-->
	</xsl:template>
	
</xsl:stylesheet>