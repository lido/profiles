<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:lido="http://www.lido-schema.org"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="1.0"
               lido:dummy="">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.1 (XSLT 1.0)</skos:prefLabel>
         </dct:Agent>
      </dct:creator>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:template match="/">
      <xsl:variable name="schxslt:report">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                                xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.1 (XSLT 1.0)</skos:prefLabel>
                     </dct:Agent>
                  </dct:creator>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
         <xsl:call-template name="d4e4"/>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                              title="A Schematron schema for the EODEM LIDO profile">
         <svrl:ns-prefix-in-attribute-values prefix="lido" uri="http://www.lido-schema.org"/>
         <xsl:copy-of select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template name="d4e4">
      <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>
      <xsl:apply-templates select="/" mode="d4e4"/>
   </xsl:template>
   <xsl:template match="lido:lido" mode="d4e4" priority="11">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:lido</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(lido:administrativeMetadata/lido:recordWrap/lido:recordID)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">lido:administrativeMetadata/lido:recordWrap/lido:recordID</xsl:attribute>
            <svrl:text>LIDO requires each record to have a local Record Identifier</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(lido:administrativeMetadata/lido:recordWrap/lido:recordInfoSet[@lido:type='http://terminology.lido-schema.org/lido00470']/lido:recordMetadataDate[@lido:type='http://terminology.lido-schema.org/lido00472'])">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">lido:administrativeMetadata/lido:recordWrap/lido:recordInfoSet[@lido:type='http://terminology.lido-schema.org/lido00470']/lido:recordMetadataDate[@lido:type='http://terminology.lido-schema.org/lido00472']</xsl:attribute>
            <svrl:text>The export date/time stamp must be included.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(lido:administrativeMetadata/lido:recordWrap/lido:recordType)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">lido:administrativeMetadata/lido:recordWrap/lido:recordType</xsl:attribute>
            <svrl:text>LIDO requires each record to have a Record Type</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:applicationProfile)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:applicationProfile)=1</xsl:attribute>
            <svrl:text>The application profile must be specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata)=1</xsl:attribute>
            <svrl:text>Exactly one descriptiveMetadata element must be specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata/lido:objectClassificationWrap/lido:objectWorkTypeWrap/lido:objectWorkType/lido:term) &gt; 0)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata/lido:objectClassificationWrap/lido:objectWorkTypeWrap/lido:objectWorkType/lido:term) &gt; 0</xsl:attribute>
            <svrl:text>At least one object/work type keyword (term) must be specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:descriptionWrap/lido:descriptionSet/lido:descriptiveNoteValue[@lido:label='brief description']) &lt; 2)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:descriptionWrap/lido:descriptionSet/lido:descriptiveNoteValue[@lido:label='brief description']) &lt; 2</xsl:attribute>
            <svrl:text>No more than one brief description should be specified</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:repositoryWrap/lido:repositorySet/lido:repositoryName/lido:legalBodyName/lido:appellationValue) = 1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:repositoryWrap/lido:repositorySet/lido:repositoryName/lido:legalBodyName/lido:appellationValue) = 1</xsl:attribute>
            <svrl:text>Exactly one lender must be specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:repositoryWrap/lido:repositorySet/lido:workID)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:repositoryWrap/lido:repositorySet/lido:workID)=1</xsl:attribute>
            <svrl:text>An EODEM record for an object contains exactly one local identifier.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:titleWrap/lido:titleSet/lido:appellationValue) &gt; 0)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:descriptiveMetadata/lido:objectIdentificationWrap/lido:titleWrap/lido:titleSet/lido:appellationValue) &gt; 0</xsl:attribute>
            <svrl:text>An EODEM record for an object must have a title/name.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(lido:lidoRecID[string(normalize-space(text()))])">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">lido:lidoRecID[string(normalize-space(text()))]</xsl:attribute>
            <svrl:text>LIDO requires each record to have a non-null Record Identifier</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:lidoRecID)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:lidoRecID)=1</xsl:attribute>
            <svrl:text>LIDO requires each record to have exactly one Record Identifier</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:objectPublishedID) &lt; 2)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:objectPublishedID) &lt; 2</xsl:attribute>
            <svrl:text>An EODEM object record contains at most one published identifier.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:appellationValue|lido:conceptID|lido:descriptiveNoteValue|lido:legalBodyID|lido:term|lido:workID"
                 mode="d4e4"
                 priority="10">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:appellationValue|lido:conceptID|lido:descriptiveNoteValue|lido:legalBodyID|lido:term|lido:workID</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(string(normalize-space(text())))">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">string(normalize-space(text()))</xsl:attribute>
            <svrl:text>
               <xsl:value-of select="name()"/> must have a non-empty value</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:conceptID|lido:legalBodyID|lido:lidoRecID|lido:workID"
                 mode="d4e4"
                 priority="9">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:conceptID|lido:legalBodyID|lido:lidoRecID|lido:workID</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(@lido:type[.='http://terminology.lido-schema.org/lido00099' or .='http://terminology.lido-schema.org/lido00100' or .='http://terminology.lido-schema.org/lido00510'])">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">@lido:type[.='http://terminology.lido-schema.org/lido00099' or .='http://terminology.lido-schema.org/lido00100' or .='http://terminology.lido-schema.org/lido00510']</xsl:attribute>
            <svrl:text>The <xsl:value-of select="name()"/> type attribute must have a LIDO URL meaning 'iri', 'local_identifier' or 'uri'.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:actor" mode="d4e4" priority="8">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:actor</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(@lido:type[.='http://terminology.lido-schema.org/lido00163' or .='http://terminology.lido-schema.org/lido00542' or .='http://terminology.lido-schema.org/lido00165' or .='http://terminology.lido-schema.org/lido00166' or .='http://terminology.lido-schema.org/lido00413'])">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">@lido:type[.='http://terminology.lido-schema.org/lido00163' or .='http://terminology.lido-schema.org/lido00542' or .='http://terminology.lido-schema.org/lido00165' or .='http://terminology.lido-schema.org/lido00166' or .='http://terminology.lido-schema.org/lido00413']</xsl:attribute>
            <svrl:text>The actor type attribute must have a LIDO URL meaning 'groups_of_persons', 'family', 'organization' or 'person'.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:nameActorSet/lido:appellationValue[@lido:label='display name' or @lido:label='sort name']) &gt; 0)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:nameActorSet/lido:appellationValue[@lido:label='display name' or @lido:label='sort name']) &gt; 0</xsl:attribute>
            <svrl:text>A display name and/or sort name must be specified for each actor.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:applicationProfile" mode="d4e4" priority="7">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:applicationProfile</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(.='https://lido-schema.org/profiles/v1.1/lido-v1.1-profile-eodem-v1.0.xsd')">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">.='https://lido-schema.org/profiles/v1.1/lido-v1.1-profile-eodem-v1.0.xsd'</xsl:attribute>
            <svrl:text>The EODEM application profile must be used</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:measurementsSet" mode="d4e4" priority="6">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:measurementsSet</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(count(lido:measurementType)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:measurementType)=1</xsl:attribute>
            <svrl:text>Each measurement set must have exactly one type.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:measurementUnit)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:measurementUnit)=1</xsl:attribute>
            <svrl:text>Each measurement set must have exactly one unit.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:measurementValue)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:measurementValue)=1</xsl:attribute>
            <svrl:text>Each measurement set must have exactly one value.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:objectMaterialsTechSet" mode="d4e4" priority="5">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:objectMaterialsTechSet</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(count(lido:displayMaterialsTech|lido:materialsTech/lido:termMaterialsTech/lido:term) &gt; 0)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:displayMaterialsTech|lido:materialsTech/lido:termMaterialsTech/lido:term) &gt; 0</xsl:attribute>
            <svrl:text>Each Material group must contain Material Description and/or one or more Material Keywords.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:objectMeasurementsSet" mode="d4e4" priority="4">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:objectMeasurementsSet</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(count(lido:displayObjectMeasurements|lido:objectMeasurements) &gt; 0)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:displayObjectMeasurements|lido:objectMeasurements) &gt; 0</xsl:attribute>
            <svrl:text>Each measurement group must contain Measurement Text and/or Dimension Aspect Group.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:objectPublishedID" mode="d4e4" priority="3">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:objectPublishedID</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(string(normalize-space(text())))">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">string(normalize-space(text()))</xsl:attribute>
            <svrl:text>A published identifier must have a value.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(@lido:type='http://terminology.lido-schema.org/lido00099')">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">@lido:type='http://terminology.lido-schema.org/lido00099'</xsl:attribute>
            <svrl:text>A published identifier @type must have the LIDO URL meaning 'uri' (http://terminology.lido-schema.org/lido00099)</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="@lido:pref" mode="d4e4" priority="2">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">@lido:pref</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(.='http://terminology.lido-schema.org/lido00169' or .='http://terminology.lido-schema.org/lido00170' or .='http://terminology.lido-schema.org/lido00426' or .='http://terminology.lido-schema.org/lido00526')">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">.='http://terminology.lido-schema.org/lido00169' or .='http://terminology.lido-schema.org/lido00170' or .='http://terminology.lido-schema.org/lido00426' or .='http://terminology.lido-schema.org/lido00526'</xsl:attribute>
            <svrl:text>The pref attribute must have a LIDO URL meaning 'alternative', 'display', 'hidden' or 'preferred'.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:repositorySet" mode="d4e4" priority="1">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:repositorySet</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(@lido:type[.='http://terminology.lido-schema.org/lido00475'])">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">@lido:type[.='http://terminology.lido-schema.org/lido00475']</xsl:attribute>
            <svrl:text>The EODEM repository type must be the LIDO URL for the current repository or location (http://terminology.lido-schema.org/lido00475)</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template match="lido:titleSet" mode="d4e4" priority="0">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:attribute name="context">lido:titleSet</xsl:attribute>
      </svrl:fired-rule>
      <xsl:if test="not(@lido:type)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">@lido:type</xsl:attribute>
            <svrl:text>Title sets must have a type specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(count(lido:appellationValue)=1)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">count(lido:appellationValue)=1</xsl:attribute>
            <svrl:text>There should be exactly one title/name per titleSet element.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:if test="not(lido:appellationValue/@xml:lang)">
         <xsl:variable xmlns:svrl="http://purl.oclc.org/dsdl/svrl" name="location">
            <xsl:call-template name="schxslt:location">
               <xsl:with-param name="node" select="."/>
            </xsl:call-template>
         </xsl:variable>
         <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                             location="{normalize-space($location)}">
            <xsl:attribute name="test">lido:appellationValue/@xml:lang</xsl:attribute>
            <svrl:text>The language of each title should be specified.</svrl:text>
         </svrl:failed-assert>
      </xsl:if>
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template mode="d4e4" match="*" priority="-10">
      <xsl:apply-templates mode="d4e4" select="node() | @*"/>
   </xsl:template>
   <xsl:template mode="d4e4" match="@* | text()" priority="-10"/>
   <xsl:template xmlns="http://www.w3.org/1999/XSL/TransformAlias"
                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                 name="schxslt:location">
      <xsl:param name="node"/>
      <xsl:variable name="path">
         <xsl:for-each select="$node/ancestor::*">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
         </xsl:for-each>
         <xsl:text>/</xsl:text>
         <xsl:variable name="position">
            <xsl:number level="single"/>
         </xsl:variable>
         <xsl:choose>
            <xsl:when test="$node/self::*">
               <xsl:value-of select="concat('Q{', namespace-uri($node), '}', local-name($node), '[', $position, ']')"/>
            </xsl:when>
            <xsl:when test="count($node/../@*) = count($node|$node/../@*)">
               <xsl:value-of select="concat('@Q{', namespace-uri($node), '}', local-name($node))"/>
            </xsl:when>
            <xsl:when test="$node/self::processing-instruction()">
               <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)', '[', $position, ']')"/>
            </xsl:when>
            <xsl:when test="$node/self::comment()">
               <xsl:value-of select="concat('comment()', '[', $position, ']')"/>
            </xsl:when>
            <xsl:when test="$node/self::text()">
               <xsl:value-of select="concat('text()', '[', $position, ']')"/>
            </xsl:when>
         </xsl:choose>
      </xsl:variable>
      <xsl:value-of select="$path"/>
   </xsl:template>
</xsl:transform>
