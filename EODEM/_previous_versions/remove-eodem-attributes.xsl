<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns:eodem="http://www.eodem-schema.org"
 exclude-result-prefixes="eodem">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
<xsl:template match="/*" priority="2">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:attribute name="xsi:schemaLocation">http://www.lido-schema.org lido-v1.1-profile-eodem-v1.0.xsd</xsl:attribute>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>
<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*" priority="-1">
		<xsl:copy/>
	</xsl:template>
	<xsl:template match="@*[namespace-uri()='http://www.eodem-schema.org']"/>
</xsl:stylesheet>
