<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    xmlns:tei="http://www.tei-c.org/ns/1.0">
  <xsl:output method="xml" encoding="UTF-8"/>
  <xsl:template match="tei:div[@type='ap_unit'][@xml:id]">
    <xsl:message>ap_unit_ID para created for <xsl:value-of select="@xml:id"/></xsl:message>
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="tei:head"/>
      <tei:div type="ap_unit_ID">
        <tei:p><xsl:value-of select="@xml:id"/></tei:p>
      </tei:div>    
      <xsl:apply-templates select="*[not(self::tei:head)]"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*" priority="-1">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>    
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>