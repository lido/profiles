# Sample Saxon commands for generating EODEM files

The Saxon XSL processor, in free and paid-for versions for multiple operating systems, can be obtained from <https://www.saxonica.com/products/products.xml>.

This document assumes that:

- Commands are run within a directory structure that replicates that of this repo
- Commands are run from the EODEM directory
- We are working with EODEM 1.0

The `-t` switch is optional; it produces more verbose output when running, which can be reassuring.

1. process **D1** with the generic LIDO **lidodd.xsl** to produce **R1**:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0.xml -xsl:../generic/lidodd.xsl -o:lido-v1.1-profile-EODEM-v1.0.xsd -expand:off -t
	```
1. process **D1** with **X2** to produce **R3**:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0.xml -xsl:display-profile-as-html.xsl -o:lido-v1.1-profile-EODEM-v1.0.html -expand:off -t
	```
1. process **D1** with **X3** to produce **R4**:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0.xml -xsl:display-profile-as-xsl-fo.xsl -o:lido-v1.1-profile-EODEM-v1.0.fo -expand:off -t
	```
1. process **R4** with an XSL-FO processor to produce **R5**: n/a (not an XSL transformation)
1. process **D1** with **X4** to produce `lido-v1.1-profile-EODEM-v1.0-sample-record-with-attributes.xml` **(T1)**, a temporary file - sample record with added profile-specific information:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0.xml -xsl:profile-to-example-record.xsl -o:lido-v1.1-profile-EODEM-v1.0-sample-record-with-attributes.xml -expand:off -t
	```
1. process **T1** with **X1** to produce **R2**:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0-sample-record-with-attributes.xml -xsl:eodem-document-structure.xsl -o:lido-v1.1-profile-EODEM-v1.0-document-structure.html -expand:off -t
    ```
    
    Note that this creates links to the relevant units of information in the EODEM HTML documentation by default; to override this, enter the name of the file you wish to link to by adding the `htmldoc` parameter, e.g. `htmldoc="lido-v1.1-profile-EODEM-v1.0.htm"`, to your command.
    
1. process **T1** with **X5** to produce **R6**:
    ```
    transform -s:lido-v1.1-profile-EODEM-v1.0-sample-record-with-attributes.xml -xsl:remove-profile-specific-attributes.xsl -o:lido-v1.1-profile-EODEM-v1.0-sample-record.xml -expand:off -t
    ```
    
    Note that this needs to be told the full namespace URI for the profile-specific namespace that you wish to remove from the outline document, as well as its location. These are set to values for EODEM by default; to override them, add the relevant values in the parameters `profile-uri`, `profile-uri-2` (in case you are using more than one profile-specific namespace) and `schema-location` to your command.
    
