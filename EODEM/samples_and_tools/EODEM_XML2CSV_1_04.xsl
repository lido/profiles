<?xml version="1.0" encoding="UTF-8"?>
<!--  Create CSV from LIDO xml. Version 1.2, Jette Klein-Berning September 2021. 
		Sample draft - multiplicity / cardinality checked for several items. 
		No guarantee that all multiplicity problems are handled completely. 
		This XSLT should serve as an example how CSV files could be created in general from a LIDO xml file. 
-->
<xsl:stylesheet version="1.0" xmlns:gml="http://www.opengis.net/gml" 
xmlns:lido="http://www.lido-schema.org" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output  encoding="utf-8" method="text" cdata-section-elements="lido:descriptiveNoteValue lido:displayObject"/>
<!--<xsl:strip-space elements="*"/>-->

<!-- general variables for the whole LIDO file-->
<!-- count maximum amount of multiplicity elements over all lido records in the current dataset-->
<xsl:variable name="maxArtists">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:eventActor)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:eventActor)"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxPeriods">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:periodName)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:periodName)"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxTitles">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:titleSet)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:titleSet)"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxMaterials1">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:objectMaterialsTechSet)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:objectMaterialsTechSet)"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxMaterials2">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:objectMaterialsTechSet/lido:materialsTech/lido:termMaterialsTech)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:objectMaterialsTechSet/lido:materialsTech/lido:termMaterialsTech)-1"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxMaterials">
	<xsl:value-of select="$maxMaterials1+$maxMaterials2"/>
</xsl:variable>

<xsl:variable name="maxMeasurements">
	<xsl:for-each select=".//lido:lido">
			<xsl:sort select="count(.//lido:objectMeasurementsSet)" data-type="number"/>
			<xsl:if test="position()= last()">
				<xsl:value-of select="count(.//lido:objectMeasurementsSet)"/>
			</xsl:if>
	</xsl:for-each>
</xsl:variable>

<!-- ...............................-->

<!-- General templates-->
	<xsl:template match="/">
		<!--<xsl:call-template name="createfirstRowTitles"/>-->
		<xsl:call-template name="createSecondRowTitles"/>
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- not needed - only one header row will be created via "createSecondRowTitles" instead-->
	<xsl:template name="createfirstRowTitles">
	<xsl:text>ObjectID,Object Number,Classification,Classification,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Constituent,Title,Title,Title,Dated,Dated,Dated,Begin ISO Date,End ISO Date,Medium,Medium,Medium,Dimensions,Dimensions,Dimensions,Dimensions,Dimensions,Dimensions,Description,,Credit Line,Alternate Number,School,Period,Stated Value,Currency
		</xsl:text>
	</xsl:template>
	
	<xsl:template name="createSecondRowTitles">
		<xsl:text>LIDO Record ID,Object Identifier,</xsl:text>
		<!--multiple--><xsl:text>Object Type Identifier,Object Type Keyword,</xsl:text>
		
		<!--<xsl:text>Max Artists: </xsl:text>-->
		<xsl:call-template name="createArtistColumns">
               <xsl:with-param name="i">1</xsl:with-param>
               <xsl:with-param name="count"><xsl:value-of select="$maxArtists"/></xsl:with-param>
            </xsl:call-template> 
		
		<xsl:text>Lender Identifier,Lender Name,</xsl:text>
		<!--<xsl:text>Max titles: </xsl:text><xsl:value-of select="$maxTitles"/>-->
		<xsl:call-template name="createTitleColumns">
			<xsl:with-param name="i">1</xsl:with-param>
			<xsl:with-param name="count"><xsl:value-of select="$maxTitles"/></xsl:with-param>
		</xsl:call-template>
		
		<xsl:text>Date Type Identifier,Date Type Keyword,Display Date,Date To,Date,</xsl:text>
		<!--<xsl:text>Max materials: </xsl:text><xsl:value-of select="$maxMaterials"/>-->
		<xsl:call-template name="createMaterialColumns">
			<xsl:with-param name="i">1</xsl:with-param>
			<xsl:with-param name="count"><xsl:value-of select="$maxMaterials"/></xsl:with-param>
		</xsl:call-template>
		
		<!--<xsl:text>Max measurements: </xsl:text><xsl:value-of select="$maxMeasurements"/>-->
		<xsl:call-template name="createMeasurementColumns">
			<xsl:with-param name="i">1</xsl:with-param>
			<xsl:with-param name="count"><xsl:value-of select="$maxMeasurements"/></xsl:with-param>
		</xsl:call-template>
		<xsl:text>Brief Description,Insurance/Indemnity,Credit Line,URI,</xsl:text>
		<!--<xsl:text>Max Periods: </xsl:text> <xsl:value-of select="$maxPeriods"/>-->
		<xsl:call-template name="createPeriodColumns">
			<xsl:with-param name="i">1</xsl:with-param>
			<xsl:with-param name="count"><xsl:value-of select="$maxPeriods"/></xsl:with-param>
		</xsl:call-template>
		
		<xsl:text>Value,Value Currency,Condition Of Object,Handling,Transport,Condition Checking,Hazards,Temperature Requirements Text,Target Minimum Temperature,Target Maximum Temperature,Maximum Temperature Variation,Minimum Temperature Variation Period,Relative Humidity Requirements Text,Target Minimum Relative Humidity,Target Maximum Relative Humidity,Maximum Relative Humidity Variation,Minimum Relative Humidity Variation Period,Visible Light Exposure Requirements Text,Maximum Light Exposure,Maximum Cumulative Light Exposure,Maximum Cumulative Light Exposure Duration,Ultraviolet Light Exposure Requirements Text,Maximum Ultraviolet Light Strength,Ideal Ultraviolet Light Strength,Upper Ultraviolet Light Wavelength Boundary,Installation Method,Display Method,Security,Photography And Filming,Local Record ID,LIDO Record Type Identifier,LIDO Record Type Keyword,Export Date/Timestamp,Image,Image Type
</xsl:text><!--  No indentation, to remove spurious tabs at beginning of data block  -->
	</xsl:template>
	
	<xsl:template name="createArtistColumns">
		<xsl:param name="i" />
        <xsl:param name="count" />
		<!--Maker Type,Maker Identifier,Maker Display Name,Maker Sort Name,Maker Earliest Date,Maker Latest Date,Maker Role Identifier,Maker Role Keyword,Attribution Qualifier-->
		<xsl:if test="$i &lt;= $count">
          <xsl:text>Maker Type </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Identifier </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Display Name </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Sort Name </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Earliest Date </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Latest Date </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Role Identifier </xsl:text><xsl:value-of select="$i"/><xsl:text>,Maker Role Keyword </xsl:text><xsl:value-of select="$i"/><xsl:text>,Attribution Qualifier </xsl:text><xsl:value-of select="$i"/><xsl:text>,</xsl:text>
        </xsl:if>
        
        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="createArtistColumns">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<xsl:template name="createPeriodColumns">
		<xsl:param name="i" />
        <xsl:param name="count" />
		<xsl:if test="$i &lt;= $count">
          <xsl:text>Period Identifier </xsl:text><xsl:value-of select="$i"/><xsl:text>,Period Keyword</xsl:text><xsl:value-of select="$i"/><xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="createPeriodColumns">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<xsl:template name="createTitleColumns">
		<xsl:param name="i" />
        <xsl:param name="count" />
		<xsl:if test="$i &lt;= $count">
          <xsl:text>Title Type </xsl:text><xsl:value-of select="$i"/><xsl:text>,Title/Name </xsl:text><xsl:value-of select="$i"/><xsl:text>,Title Language </xsl:text><xsl:value-of select="$i"/><xsl:text>,</xsl:text>
        </xsl:if>

        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="createTitleColumns">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<xsl:template name="createMaterialColumns">
		<xsl:param name="i" />
        <xsl:param name="count" />
		<xsl:if test="$i &lt;= $count">
          <xsl:text>Material Description </xsl:text><xsl:value-of select="$i"/><xsl:text>,Material Identifier </xsl:text><xsl:value-of select="$i"/><xsl:text>,Material Keyword </xsl:text><xsl:value-of select="$i"/><xsl:text>,</xsl:text>
        </xsl:if>

        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="createMaterialColumns">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<xsl:template name="createMeasurementColumns">
		<xsl:param name="i" />
        <xsl:param name="count" />
		<xsl:if test="$i &lt;= $count">
          <xsl:text>Measurement Text </xsl:text><xsl:value-of select="$i"/><xsl:text>,Dimension </xsl:text><xsl:value-of select="$i"/><xsl:text>,Unit </xsl:text><xsl:value-of select="$i"/><xsl:text>,Measurement </xsl:text><xsl:value-of select="$i"/><xsl:text>,Dimension Aspect </xsl:text><xsl:value-of select="$i"/><xsl:text>,Dimension Qualifier</xsl:text><xsl:value-of select="$i"/><xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="createMeasurementColumns">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<!-- separators (comma) must be created in case of missing subelements or completely missing 
	elements to fulfill the correct CSV structure for all LIDO records in a CSV file-->
	<xsl:template name="createSeparator">
		<xsl:param name="i"/>
		<xsl:param name="count"/>
		<xsl:param name="max"/>
		<xsl:if test="$i &lt; $count">
			<!-- max. 9 elements per eventActor-->
			<xsl:value-of select="$max"/>
            <xsl:call-template name="createSeparator">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
				<xsl:with-param name="max"><xsl:value-of select="$max"/></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	
	<xsl:template match="lido:lido">
		<!-- amount of eventActors in current LIDO record-->
		<xsl:variable name="maxLidoArtists">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:eventActor)" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:eventActor)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="maxLidoPeriods">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:periodName)" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:periodName)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="maxLidoTitles">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:titleSet)" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:titleSet)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:variable name="maxLidoMaterials1">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:objectMaterialsTechSet[lido:displayMaterialsTech])" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:objectMaterialsTechSet[lido:displayMaterialsTech])"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="maxLidoMaterials2">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:objectMaterialsTechSet/lido:materialsTech/lido:termMaterialsTech)" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:objectMaterialsTechSet/lido:materialsTech/lido:termMaterialsTech)-1"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="maxLidoMaterials">
			<xsl:value-of select="$maxLidoMaterials1+$maxLidoMaterials2"/>
		</xsl:variable>
		
		<xsl:variable name="maxLidoMeasurements">
			<xsl:for-each select=".">
				<xsl:sort select="count(.//lido:objectMeasurementsSet)" data-type="number"/>
				<xsl:if test="position()= last()">
					<xsl:value-of select="count(.//lido:objectMeasurementsSet)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!--new line to be inserted into CSV for each LIDO record in file-->
		<xsl:if test="preceding-sibling::lido:lido"><xsl:text>
</xsl:text>
</xsl:if>
<!-- ..................................................................-->
<!-- Create columns in order of example CSV file for all EODEM contents-->
<!-- ..................................................................-->
<!-- LIDO Record ID-->
<xsl:apply-templates select="lido:lidoRecID"/><xsl:text>,</xsl:text>
<!-- Object Identifier-->
<xsl:apply-templates select=".//lido:repositoryWrap/lido:repositorySet/lido:workID"/><xsl:text>,</xsl:text>
<!-- Object Type Identifier-->
<xsl:apply-templates select=".//lido:objectWorkType/lido:conceptID"/><xsl:text>,</xsl:text>
<!-- Object Type Keyword-->
<xsl:apply-templates select=".//lido:objectWorkType/lido:term"/><xsl:text>,</xsl:text>
<!-- Maker-->
<xsl:if test="$maxLidoArtists&gt;0">
	<xsl:apply-templates select=".//lido:eventActor"/><xsl:text>,</xsl:text>
</xsl:if>
<xsl:call-template name="createSeparator">
	<xsl:with-param name="i"><xsl:value-of select="$maxLidoArtists"/></xsl:with-param>
	<xsl:with-param name="count"><xsl:value-of select="$maxArtists"/></xsl:with-param>
	<xsl:with-param name="max">,,,,,,,,,</xsl:with-param>	
</xsl:call-template>

<!-- Lender Identifier-->
<xsl:apply-templates select=".//lido:repositorySet/lido:repositoryName/lido:legalBodyID"/><xsl:text>,</xsl:text>
<!-- Lender Name-->
<xsl:apply-templates select=".//lido:repositorySet/lido:repositoryName/lido:legalBodyName/lido:appellationValue"/><xsl:text>,</xsl:text>
<!-- Title details (m:n)-->
<xsl:apply-templates select=".//lido:titleSet"/><xsl:text>,</xsl:text>

<xsl:call-template name="createSeparator">
	<xsl:with-param name="i"><xsl:value-of select="$maxLidoTitles"/></xsl:with-param>
	<xsl:with-param name="count"><xsl:value-of select="$maxTitles"/></xsl:with-param>
	<xsl:with-param name="max">,,,</xsl:with-param>
</xsl:call-template>

<!-- Date Type Identifier -->
<xsl:apply-templates select=".//lido:eventSet/lido:event[lido:eventDate]/lido:eventType/lido:conceptID"/><xsl:text>,</xsl:text>
<!-- Date Type Keyword-->
<xsl:apply-templates select=".//lido:eventSet/lido:event[lido:eventDate]/lido:eventType/lido:term"/><xsl:text>,</xsl:text>
<!-- Date Display-->
<xsl:apply-templates select=".//lido:eventDate/lido:displayDate"/><xsl:text>,</xsl:text>
<!-- Date -->
<xsl:apply-templates select=".//lido:eventDate/lido:date/lido:earliestDate"/><xsl:text>,</xsl:text>
<!-- Date To-->
<xsl:apply-templates select=".//lido:eventDate/lido:date/lido:latestDate"/><xsl:text>,</xsl:text>

<!-- Material Description, m:n cardinality-->
<xsl:if test="$maxLidoMaterials&gt;0">
	<xsl:apply-templates select=".//lido:objectMaterialsTechWrap/lido:objectMaterialsTechSet"/><xsl:text>,</xsl:text>
</xsl:if>
<!--<xsl:text>MaxLidoMaterials: </xsl:text><xsl:value-of select="$maxLidoMaterials"/><xsl:text> + maxMaterials: </xsl:text><xsl:value-of select="$maxMaterials"/>-->

<xsl:call-template name="createSeparator">
	<xsl:with-param name="i"><xsl:value-of select="$maxLidoMaterials"/></xsl:with-param>
	<xsl:with-param name="count"><xsl:value-of select="$maxMaterials"/></xsl:with-param>
	<xsl:with-param name="max">,,,</xsl:with-param>
</xsl:call-template>

<!-- Measurements, m:n cardinality-->
<xsl:if test="$maxLidoMeasurements&gt;0">
	<xsl:apply-templates select=".//lido:objectMeasurementsWrap/lido:objectMeasurementsSet"/><xsl:text>,</xsl:text>
</xsl:if>
<xsl:call-template name="createSeparator">
	<xsl:with-param name="i"><xsl:value-of select="$maxLidoMeasurements"/></xsl:with-param>
	<xsl:with-param name="count"><xsl:value-of select="$maxMeasurements"/></xsl:with-param>
	<xsl:with-param name="max">,,,,,,</xsl:with-param>
</xsl:call-template>

<!-- Brief Description-->
<xsl:apply-templates select=".//lido:objectDescriptionSet/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>

<!-- Insurance/Indemnity-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/insurance-or-indemnity-requirements']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>

<!-- Creditline-->
<xsl:apply-templates select=".//lido:rightsWorkSet/lido:creditLine"/><xsl:text>,</xsl:text>

<!-- URI -->
<xsl:apply-templates select=".//lido:objectPublishedID"/><xsl:text>,</xsl:text>

<!-- period-->
<xsl:if test="$maxLidoPeriods&gt;0">
	<xsl:apply-templates select=".//lido:periodName"/><xsl:text>,</xsl:text>
</xsl:if>
<xsl:if test="preceding-sibling::lido:periodName"><xsl:text>,</xsl:text></xsl:if>
<xsl:call-template name="createSeparator">
	<xsl:with-param name="i"><xsl:value-of select="$maxLidoPeriods"/></xsl:with-param>
	<xsl:with-param name="count"><xsl:value-of select="$maxPeriods"/></xsl:with-param>
	<xsl:with-param name="max">,,</xsl:with-param>
</xsl:call-template>

<!-- Value-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00927' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300435426']/lido:objectMeasurements/lido:measurementsSet/lido:measurementValue"/><xsl:text>,</xsl:text>
<!-- Value Currency-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00927' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300435426']/lido:objectMeasurements/lido:measurementsSet/lido:measurementUnit"/><xsl:text>,</xsl:text>

<!-- Condition of Object-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/condition-note']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Handling-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/handling-recommendations']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Transport-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/shipping-note']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Condition Checking-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/condition-checking-requirements']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Hazards -->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/hazards-note']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- .............................-->


<!-- Temperature requirements Text-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/lido:displayObjectMeasurements"/><xsl:text>,</xsl:text>
<!-- Target minimum Temperature-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443984']"/><xsl:text>,</xsl:text>
<!-- Target maximum Temperature-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443983']"/><xsl:text>,</xsl:text>
<!-- Maximum Temperature variation-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443559']"/><xsl:text>,</xsl:text>
<!-- Minimum Temperature Variation Period-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443981']"/><xsl:text>,</xsl:text>

<!-- Relative Humidity requirements-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/lido:displayObjectMeasurements"/><xsl:text>,</xsl:text>
<!-- Target minimum relative humidity-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443984']"/><xsl:text>,</xsl:text>
<!-- Target maximum relative humidity-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443983']"/><xsl:text>,</xsl:text>
<!-- Maximum relative humidity variation period-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443985']"/><xsl:text>,</xsl:text>
<!-- Minimum relative humidity Variation -->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443981']"/><xsl:text>,</xsl:text>

<!-- Visible light exposure requirements-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/lido:displayObjectMeasurements"/><xsl:text>,</xsl:text>
<!-- Maximum Visible light exposure -->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300266203']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='maximum visible light exposure']"/><xsl:text>,</xsl:text>-->
<!-- Maximum Cumulative Visible light exposure -->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443982']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='maximum cumulative visible light exposure']"/><xsl:text>,</xsl:text>-->
<!-- Maximum Cumulative Visible light exposure duration-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' 
							and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443981']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='maximum cumulative visible light exposure duration']"/><xsl:text>,</xsl:text>-->

<!-- Ultraviolet light exposure requirements-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/lido:displayObjectMeasurements"/><xsl:text>,</xsl:text>
<!-- Maximum UV light exposure -->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443983']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='http://vocab.getty.edu/aat/300443983']"/><xsl:text>,</xsl:text>-->
<!-- target ultraviolet light exposure -->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443986']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='target ultraviolet light exposure']"/><xsl:text>,</xsl:text>-->
<!-- upper ultraviolet light wavelength boundary-->
<xsl:apply-templates select=".//lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/lido:objectMeasurements/
							lido:measurementsSet[lido:measurementType/lido:conceptID/@lido:type='http://terminology.lido-schema.org/lido00099' and lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443987']"/><xsl:text>,</xsl:text>
							<!--lido:measurementsSet[lido:measurementType='upper ultraviolet light wavelength boundary']"/><xsl:text>,</xsl:text>-->

<!-- Installation method-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/installation-note']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Display method-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/display-recommendations']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Security-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/security-requirements']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>
<!-- Photography and Filming-->
<xsl:apply-templates select=".//lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/imaging-requirements']/lido:descriptiveNoteValue"/><xsl:text>,</xsl:text>

<!-- Local Record ID -->
<xsl:apply-templates select=".//lido:recordWrap/lido:recordID"/><xsl:text>,</xsl:text>

<!-- LIDO Record Type Identifier-->
<xsl:apply-templates select=".//lido:recordWrap/lido:recordType/lido:conceptID"/><xsl:text>,</xsl:text>
<!-- LIDO Record Type Keyword-->
<xsl:apply-templates select=".//lido:recordWrap/lido:recordType/lido:term"/><xsl:text>,</xsl:text>

<!-- Export Date/Timestamp-->
<xsl:apply-templates select=".//lido:recordMetadataDate"/><xsl:text>,</xsl:text>

<!-- Image -->
<xsl:apply-templates select=".//lido:linkResource"/><xsl:text>,</xsl:text>
<!-- Image Type-->
<xsl:apply-templates select=".//lido:linkResource/@lido:formatResource"/>
	</xsl:template>
		
	<!-- template for single values-->
	<xsl:template match="lido:lidoRecID|
						lido:repositorySet/lido:workID|
						lido:repositorySet/lido:repositoryName/lido:legalBodyName/lido:appellationValue|
						lido:eventType/lido:term|
						lido:eventSet/lido:event[lido:eventDate]/lido:eventType/lido:conceptID|
						lido:eventSet/lido:event[lido:eventDate]/lido:eventType/lido:term|
						lido:eventDate/lido:displayDate|
						lido:eventDate/lido:date/lido:earliestDate|
						lido:eventDate/lido:date/lido:latestDate|
						lido:objectDescriptionSet/lido:descriptiveNoteValue|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/insurance-or-indemnity-requirements' or 
							@lido:type='http://purl.org/spectrum/units/installation-note' or 
							@lido:type='http://purl.org/spectrum/units/display-recommendations' or
							@lido:type='http://purl.org/spectrum/units/security-requirements' or 
							@lido:type='http://purl.org/spectrum/units/imaging-requirements']/lido:descriptiveNoteValue|
						lido:rightsWorkSet/lido:creditLine|
						lido:objectPublishedID|
						lido:eventDescriptionSet[@lido:type='valuation']/lido:descriptiveNoteValue[@lido:label='value']|
						lido:eventDescriptionSet[@lido:type='valuation']/lido:descriptiveNoteValue[@lido:label='currency']|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/condition-note']/lido:descriptiveNoteValue|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/handling-recommendations']/lido:descriptiveNoteValue|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/shipping-note']/lido:descriptiveNoteValue|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/condition-checking-requirements']/lido:descriptiveNoteValue|
						lido:eventDescriptionSet[@lido:type='http://purl.org/spectrum/units/hazards-note']/lido:descriptiveNoteValue|
						lido:recordWrap/lido:recordID|
						lido:recordType/lido:conceptID|
						lido:recordType/lido:term|
						lido:recordMetadataDate">
						
		<xsl:text>"</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
	</xsl:template>
	
	<!-- maker details m:n cardinality-->
	<xsl:template match="lido:eventActor">
		<xsl:if test="preceding-sibling::lido:eventActor"><xsl:text>,</xsl:text></xsl:if>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/@lido:type"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/lido:actorID"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/lido:nameActorSet/lido:appellationValue[@lido:label='display name']"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/lido:nameActorSet/lido:appellationValue[@lido:label='sort name']"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:earliestDate"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:latestDate"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:roleActor/lido:conceptID"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:roleActor/lido:term"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:actorInRole/lido:attributionQualifierActor"/>
	</xsl:template>
	
	<!-- Title, m:n cardinality-->
	<xsl:template match="lido:titleSet">
		<xsl:if test="preceding-sibling::lido:titleSet"><xsl:text>,</xsl:text></xsl:if>
		<xsl:apply-templates select="@lido:type"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:appellationValue"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:appellationValue/@xml:lang"/>
	</xsl:template>
	
	<!-- Materials/techniques, m:n cardinality-->
	<xsl:template match="lido:objectMaterialsTechSet">
		<xsl:if test="preceding-sibling::lido:objectMaterialsTechSet"><xsl:text>,</xsl:text></xsl:if>
		<!-- 20210715-->
		<xsl:apply-templates select="lido:displayMaterialsTech"/><xsl:text>,</xsl:text>
		<xsl:choose> 
			<xsl:when test="lido:materialsTech/lido:termMaterialsTech">
				<xsl:apply-templates select="lido:materialsTech/lido:termMaterialsTech"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>,</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="lido:materialsTech/lido:termMaterialsTech">
		<xsl:if test="preceding-sibling::lido:termMaterialsTech"><xsl:text>,</xsl:text>
			<xsl:apply-templates select="parent::lido:materialsTech/preceding-sibling::lido:displayMaterialsTech"/><xsl:text>,</xsl:text>
		</xsl:if>
		
		
		
		<xsl:apply-templates select="lido:conceptID"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:term"/>		
	</xsl:template>
	
	<!-- object measurements, m:n cardinality-->
	<xsl:template match="lido:objectMeasurementsSet">
		<xsl:if test="preceding-sibling::lido:objectMeasurementsSet"><xsl:text>,</xsl:text></xsl:if>
		<xsl:apply-templates select="lido:displayObjectMeasurements"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:objectMeasurements/lido:measurementsSet/lido:measurementValue"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:objectMeasurements/lido:measurementsSet/lido:measurementType"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:objectMeasurements/lido:measurementsSet/lido:measurementUnit"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:objectMeasurements/lido:extentMeasurements"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:objectMeasurements/lido:qualifierMeasurements"/>
	</xsl:template>
	
	<!-- periodName, m:n cardinality-->
	<xsl:template match="lido:periodName">
		<xsl:if test="preceding-sibling::lido:periodName"><xsl:text>,</xsl:text></xsl:if>
		<xsl:apply-templates select="lido:conceptID"/><xsl:text>,</xsl:text>
		<xsl:apply-templates select="lido:term"/>
	</xsl:template>
	
	<!-- template for elements with multiplicity-->
	
	<xsl:template match="lido:actorInRole/lido:roleActor/lido:conceptID|
						 lido:actorInRole/lido:actor/lido:actorID|
						 lido:actorInRole/lido:actor/@lido:type|
						 lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:earliestDate|
						 lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:latestDate|
						 lido:actorInRole/lido:roleActor/lido:term|
						 lido:actorInRole/lido:attributionQualifierActor|
						 lido:actorInRole/lido:actor/lido:nameActorSet/lido:appellationValue|
						 lido:titleSet/@lido:type|
						 lido:titleSet/lido:appellationValue|
						 lido:titleSet/lido:appellationValue/@xml:lang|
						 lido:objectWorkTypeWrap/lido:objectWorkType/lido:conceptID|
						 lido:objectWorkTypeWrap/lido:objectWorkType/lido:term|
						 lido:repositorySet/lido:repositoryName/lido:legalBodyID|
						 lido:objectMaterialsTechSet/lido:displayMaterialsTech|
						 lido:displayObjectMeasurements|
						 lido:extentMeasurements|
						 lido:qualifierMeasurements|
						 lido:objectMeasurements/lido:measurementsSet/lido:measurementValue|
						 lido:objectMeasurements/lido:measurementsSet/lido:measurementUnit|
						 lido:objectMeasurements/lido:measurementsSet/lido:measurementType|
						 lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									(@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066' or 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097' or
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024' or
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983')]
									/lido:displayObjectMeasurements|
						lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00927' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300435426']/lido:objectMeasurements/lido:measurementsSet/measurementUnit |
						lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00927' and 
							@lido:measurementsGroup='http://vocab.getty.edu/aat/300435426']/lido:objectMeasurements/lido:measurementsSet/measurementValue |
						lido:objectMeasurements/lido:measurementsSet[lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443984' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443983' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443559' or
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443981' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300266203' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443982' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443986' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443987' or 
								lido:measurementType/lido:conceptID='http://vocab.getty.edu/aat/300443985']|
						lido:displayMaterialsTech|
						lido:materialsTech/lido:termMaterialsTech/lido:conceptID|
						lido:materialsTech/lido:termMaterialsTech/lido:term|
						lido:linkResource|
						lido:linkResource/@lido:formatResource|
						lido:periodName/lido:conceptID|
						lido:periodName/lido:term">
						
		
			<xsl:if  test="(name()='lido:actorID' and preceding-sibling::lido:actorID) or 
						   (name()='lido:attributionQualifierActor' and preceding-sibling::lido:attributionQualifierActor) or 
						   (name()='lido:appellationValue' and parent::lido:titleSet and preceding-sibling::lido:appellationValue) or
						   (name()='lido:displayMaterialsTech' and preceding-sibling::lido:displayMaterialsTech) or
						   (name()='lido:measurementType' and parent::lido:measurementsSet/preceding-sibling::lido:measurementsSet/lido:measurementType) or
						   (name()='lido:measurementValue' and parent::lido:measurementsSet/preceding-sibling::lido:measurementsSet/lido:measurementValue) or 
						   (name()='lido:measurementUnit' and parent::lido:measurementsSet/preceding-sibling::lido:measurementsSet/lido:measurementUnit)">
				<xsl:text>|</xsl:text>
			</xsl:if>
			<xsl:if test="(name()='lido:conceptID' and preceding-sibling::lido:conceptID) or 
						  (name()='lido:term' and preceding-sibling::lido:term) or 
						  (name()='lido:legalBodyID' and preceding-sibling::lido:legalBodyID) or 
						  (name()='lido:linkResource' and preceding-sibling::lido:linkResource) or 
						  (name()='lido:formatResource' and parent::lido:linkResource/preceding-sibling::formatResource/@lido:formatResource)">
				<xsl:text>|</xsl:text>
			</xsl:if>
			<xsl:if test="(name()='lido:displayObjectMeasurements' and 
									parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/
									preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300266203']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300266203']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443982']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443982']) or 
									
						  (name()='lido:displayObjectMeasurements' and parent::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443981']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443981']) or 
									
						  (name()='lido:displayObjectMeasurements' and ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443986']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443986']) or 
						  (name()='lido:displayObjectMeasurements' and ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443987']/preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443987'])			
									">
				<xsl:text>|</xsl:text>
			</xsl:if>
			<!-- NEW version-->
			<xsl:if test="name()='lido:measurementsSet'">
				<xsl:if test="(ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066']/
									preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and  
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056066'])

									or 
								(ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']/
									preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and  
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300192097']	)
									or 
								(ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']/
									preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and  
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300056024']	)	
										
									or
								(ancestor::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and 
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']/
									preceding-sibling::lido:eventObjectMeasurements[@lido:type='http://terminology.lido-schema.org/lido00923' and  
									@lido:measurementsGroup='http://vocab.getty.edu/aat/300443983']	)">
					<xsl:text>|</xsl:text>
				</xsl:if>
			</xsl:if>
			
		<xsl:choose>
			<xsl:when test="name()='lido:measurementsSet'">	
				<xsl:text>"</xsl:text><xsl:value-of select="concat(lido:measurementValue, ' ', lido:measurementUnit)"/><xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>"</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
					
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
		
	
	<!-- only export explicit values-->
	
	<xsl:template match="text()"/>
	
		
</xsl:stylesheet>
