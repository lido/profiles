# EODEM samples and tools

These files are provided as aids to developers hoping to implement EODEM importers or exporters.

## Folder contents

### A rich sample record

- `EODEM_sample_record_….xml`: a sample LIDO record reflecting the
EODEM profile in its current form (version 1.00). Whilst the record is based to some extent on a painting in the National Gallery in London, much of the data is made up, and must not be taken as an accurate representation either of the painting in question, or of the National Gallery’s loan terms and
conditions.
- `N-0035-00-000177-wpu.jpg`: an image file to accompany the sample record: Titian, *Bacchus and Ariadne* © The National Gallery, London. Bought, 1826. This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License (CC BY-NC-ND 4.0) https://creativecommons.org/licenses/by-nc-nd/4.0/.
- `EODEM_sample_record_…__README.pdf`: a PDF README used when circulating the sample file outside this repository.

### A stylesheet to covert an EODEM file to CSV

- `EODEM_XML2CSV_….xsl`, an XSLT template that transforms an
EODEM-compliant LIDO XML file into a CSV file
- `EODEM_XML2CSV_…__sample_output.csv`, a copy of the output derived when applying the template to the sample EODEM XML file listed above
- `EODEM_XML2CSV_…__README.pdf`: a PDF README used when circulating these files outside this repository.

The template is not intended as a working tool, and must not be used as such. It is meant to provide a demonstration of the ways in which XSLT can be used to transform an EODEM LIDO XML file into a structure closer to that employed by collections management systems.

Similarly, because XML is a nested data structure and can allow multiple values for particular elements, whilst CSV is a flat file format, we do not recommend the use of the output CSV (or any other) in EODEM data import tools: it should be easier, in the long run, to import direct from the XML file.

Note that the only valid format for a true EODEM record is XML.

The output CSV file will by default be encoded in UTF-8: bear this in mind when opening it in tools such as MS Excel (LibreOffice Calc imports the document more smoothly). Note that EODEM treats all data values as text, whether they contain numeric values or not. Hard returns in CDATA have been manually replaced with `¬` to avoid any problems reading the CSV. There are several areas where data repeats, and these have been treated differently, to give an idea of how the problem of nested / repeating data might be addressed:

- For people / organizations, columns have been repeated
- For object measurements, columns have been repeated
- For object requirements, data has been concatenated using a `|`
- For multiple conceptIDs, data has been concatenated using a `|`

## Acknowledgements

Thank you to Jette Klein-Berning, who developed the XML2CSV stylesheet.
