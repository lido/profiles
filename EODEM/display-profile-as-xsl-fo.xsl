<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:ahfo="http://www.antennahouse.com/names/XSL/Extensions"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:profile="http://www.lido-schema.org/lidoProfile/" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="profile tei">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:param name="two_columns" select="'no'"/>
    <xsl:param name="page_orientation" select="'portrait'"/>
    <xsl:param name="colour-or-not" select="'yes'"/>
    <xsl:param name="_fontname" select="'Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif'"/>
    <xsl:param name="_fontsize" select="12"/>
    <xsl:param name="_fontbold" select="'false'"/>
    <xsl:param name="_fontitalic" select="'false'"/>
    <xsl:param name="_fontunderline" select="'false'"/>
    <xsl:param name="_fontcolor" select="'#000000'"/>
	<!--PDF header:  none by default but can be set at runtime-->
  	<xsl:param name="HeaderLeft">&#160;</xsl:param>
  	<xsl:param name="HeaderRight">&#160;</xsl:param>
  	<xsl:param name="HeaderCentre">&#160;</xsl:param>
  	<xsl:variable name="headertext"
    select="concat($HeaderLeft,$HeaderRight,$HeaderCentre)"/>
  	
	<xsl:variable name="uc" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<xsl:variable name="lc" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="quot">&quot;</xsl:variable>
	<xsl:variable name="apos">&apos;</xsl:variable>
	
<xsl:variable name="pdfopts">
	<PaperSize>a4</PaperSize>
	<Orientation>portrait</Orientation>
	<Margins>
		<Top>2.54cm</Top>
		<Bottom>1.27cm</Bottom>
		<Left>2.54cm</Left>
		<Right>2.54cm</Right>
	</Margins>
	<MarginsDoubleSided>
		<DoubleSidedMode>yes</DoubleSidedMode>
		<MarginInside>3.54cm</MarginInside>
		<MarginOutside>2.54cm</MarginOutside>
	</MarginsDoubleSided>
	<FontFaces>Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif</FontFaces>
	<FontFallback>sans-serif</FontFallback>
	<FontSize>12pt</FontSize>
	<Footers>
		<Left>@@@logo@@@</Left>
		<Right>Page @@@page@@@</Right>
		<Centre>@@@filedescription@@@</Centre>
	</Footers>
</xsl:variable>

	<xsl:template match="/">
		<fo:root>
			<xsl:attribute name="font-family"><xsl:value-of select="$_fontname"/></xsl:attribute>
			<xsl:attribute name="font-size"><xsl:value-of select="concat($_fontsize div 12, 'em')"/></xsl:attribute>
			<xsl:call-template name="pdf-pagesetup">
				<xsl:with-param name="MarginTop">1.9cm</xsl:with-param>
				<xsl:with-param name="MarginBottom">0.75cm</xsl:with-param>
				<xsl:with-param name="IncludeHeaderline">no</xsl:with-param>
				<xsl:with-param name="Orientation">
					<xsl:choose>
						<xsl:when test="$two_columns = 'yes'">landscape</xsl:when>
						<xsl:otherwise><xsl:value-of select="$page_orientation"/></xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
			<fo:page-sequence master-reference="all">
              <fo:static-content flow-name="xsl-footnote-separator">
                <fo:block text-align-last="justify">
                  <fo:leader leader-length="50%" rule-thickness="0.5pt" leader-pattern="rule"/>
                </fo:block>
              </fo:static-content>
              <xsl:call-template name="pdf-footersection"/>
				<fo:flow flow-name="xsl-region-body">
                  <!--Front matter-->                  
                  <fo:block>
                    <!--Title page-->
                    <fo:block>
                      <xsl:call-template name="display-eodem-logo"/>
                      <!--xsl:apply-templates select="//tei:teiHeader/tei:fileDesc"/-->
                      <xsl:apply-templates select="profile:lidoProfile/profile:schemaMeta" mode="front-matter"/>
                      <xsl:call-template name="display-lido-logo"/>
                      <!--xsl:call-template name="heading">
                        <xsl:with-param name="s" select="profile:lidoProfile/profile:title"/>
                        <xsl:with-param name="level" select="1"/>
                      </xsl:call-template-->                      
                    </fo:block>
                    <!--changes log-->
                    <fo:block page-break-before="always">
                      <xsl:apply-templates select="profile:lidoProfile/profile:schemaMeta/profile:revision"/>
                      <!--xsl:apply-templates select="//tei:teiHeader/tei:revisionDesc"/-->
                    </fo:block>
                    <!--Contents page-->
                    <xsl:call-template name="contents-list"/>
                    <!--Front matter-->
                    <fo:block page-break-before="always">
                      <!--xsl:apply-templates select="//tei:front"/-->
                      <xsl:apply-templates select="profile:lidoProfile/profile:schemaDoc/tei:TEI/tei:text/tei:body/tei:div[@type='guideline_introduction']"/>
                    </fo:block>
                    <!--Elements-->
                    <fo:block page-break-before="always">
                      <xsl:call-template name="heading">
                        <xsl:with-param name="s" select="'Elements'"/>
                        <xsl:with-param name="level" select="1"/>
                      </xsl:call-template>
                      <xsl:apply-templates select="//tei:div[@type='ap_unit'][.//tei:div[@type='ap_structure']/*]" mode="listing"><xsl:with-param name="elts" select="true()"/></xsl:apply-templates>
                    </fo:block>
                    <!--Attributes-->
                    <fo:block page-break-before="always">
                      <xsl:call-template name="heading">
                        <xsl:with-param name="s" select="'Attributes'"/>
                        <xsl:with-param name="level" select="1"/>
                      </xsl:call-template>
                      <xsl:apply-templates select="//tei:div[@type='ap_unit'][not(.//tei:div[@type='ap_structure'])]" mode="listing"><xsl:with-param name="elts" select="true()"/></xsl:apply-templates>
                    </fo:block>
                    <!--Indexes-->
                    <fo:block page-break-before="always">
                      <xsl:call-template name="heading">
                        <xsl:with-param name="s" select="'Indexes'"/>
                        <xsl:with-param name="level" select="1"/>
                      </xsl:call-template>
                      <xsl:call-template name="heading">
                        <xsl:with-param name="level" select="2"/>
                        <xsl:with-param name="s" select="'Elements index'"/>
                      </xsl:call-template>
                      <xsl:apply-templates select="//tei:div[@type='ap_unit'][.//tei:div[@type='ap_structure']/*]" mode="toc">
                        <xsl:sort select="translate(./tei:div[@type='ap_unit_ID']/tei:p/text(), $uc, $lc)"/>
                        <!--xsl:sort select="translate(@xml:id, $uc, $lc)"/-->
                      </xsl:apply-templates>
                      <xsl:call-template name="heading">
                        <xsl:with-param name="s" select="'Attributes index'"/>
                        <xsl:with-param name="level" select="1"/>
                      </xsl:call-template>
                      <xsl:apply-templates select="//tei:div[@type='ap_unit'][not(.//tei:div[@type='ap_structure'])]" mode="toc">
                        <xsl:sort select="./tei:div[@type='ap_unit_ID']/tei:p/text()"/>
                        <!--xsl:sort select="@xml:id"/-->
                      </xsl:apply-templates>
                    </fo:block>
                  </fo:block>
                </fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
<xsl:template match="*" mode="listing">
  <xsl:param name="elts" select="false()"/>
  <xsl:variable name="unit_id" select="tei:div[@type='ap_unit_ID']/tei:p"/>
  <xsl:variable name="heading">
    <xsl:choose>
      <xsl:when test="$elts"><xsl:value-of select="tei:head"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="@xml:id"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <fo:block id="{$unit_id}">
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="$heading"/>
      <xsl:with-param name="level" select="2"/>
    </xsl:call-template>
    <xsl:apply-templates select="tei:div[@type='ap_definition']" mode="sublist"/>
    <xsl:apply-templates select="$unit_id" mode="sublist"/>
    <xsl:apply-templates select="tei:div[not(@type='ap_definition' or @type='ap_unit_ID')]" mode="sublist"/>
  </fo:block>
</xsl:template>
	
<xsl:template match="tei:head" priority="-1">
  <xsl:call-template name="heading">
    <!--xsl:with-param name="s"/-->
    <xsl:with-param name="level" select="2"/>
  </xsl:call-template>
  <!--h2><xsl:apply-templates/></h2-->
</xsl:template>
	
<xsl:template match="tei:div[@type='guideline_introduction']/tei:head">
<!--xsl:template match="tei:front/tei:head"-->
  <xsl:call-template name="heading">
    <!--xsl:with-param name="s"/-->
    <xsl:with-param name="level" select="2"/>
  </xsl:call-template>
  <!--h2 style="text-align: center;"><xsl:apply-templates/></h2-->
</xsl:template>

<xsl:template match="tei:div[@type='guideline_introduction']/tei:div/tei:div/tei:head">
  <fo:block>
    <xsl:call-template name="heading">
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>		  
  </fo:block>
</xsl:template>

<xsl:template match="tei:div[@type='guideline_introduction']/tei:div/tei:div/tei:div/tei:head">
  <fo:block>
    <xsl:call-template name="heading">
      <xsl:with-param name="level" select="4"/>
    </xsl:call-template>		  
  </fo:block>
</xsl:template>
	
<xsl:template match="tei:head" mode="sublist">
  <fo:block>
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="'Label'"/>
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>		  
    <fo:block>
      <xsl:value-of select="text()"/>
    </fo:block>
  </fo:block>
</xsl:template>
	
<xsl:template match="@xml:id|tei:p" mode="sublist">
  <fo:block>
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="'Unit ID'"/>
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>		  
    <fo:block>
      <xsl:value-of select="."/>
    </fo:block>
  </fo:block>
</xsl:template>
	
<xsl:template match="tei:div" priority="-1">
  <fo:block margin-right="20px">
    <xsl:if test="string(@xml:id)">
      <xsl:attribute name="id"><xsl:value-of select="@xml:id"/></xsl:attribute>
    </xsl:if>
    <xsl:apply-templates/>
  </fo:block>
</xsl:template>

<!--amended to support ap_unit_ID div as place to record AP unit IDs:-->
<xsl:template match="tei:div" mode="toc">
  <xsl:if test="string(tei:head)">
    <xsl:variable name="div_id">
      <xsl:choose>
        <xsl:when test="string(tei:div[@type='ap_unit_ID']/tei:p)">
          <xsl:value-of select="tei:div[@type='ap_unit_ID']/tei:p/text()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@xml:id"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <fo:block text-align-last="justify">
      <fo:basic-link internal-destination="{$div_id}">
        <xsl:value-of select="tei:head"/>
        <!--xsl:value-of select="concat(' (', tei:div[@type='ap_unit_ID']/tei:p/text(), ')')"/-->
      </fo:basic-link>
      <xsl:text> </xsl:text>
      <fo:leader leader-length.minimum="12pt" leader-length.optimum="40pt"
        leader-length.maximum="100%" leader-pattern="dots"/>
      <xsl:text> </xsl:text>
      <fo:page-number-citation ref-id="{$div_id}"/>
    </fo:block>  
  </xsl:if>
</xsl:template>

<xsl:template match="tei:div[not(node())]" mode="sublist" priority="2"/>

<xsl:template match="tei:div" mode="sublist" priority="-1">
  <xsl:variable name="heading">
    <xsl:call-template name="div-type-as-heading"/>
  </xsl:variable>
  <fo:block>
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="$heading"/>
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>
    <fo:block>
      <xsl:apply-templates mode="subsublist"/>
    </fo:block>
  </fo:block>
</xsl:template>
	
<xsl:template match="tei:div[@type='ap_definition']" mode="sublist">
  <fo:block margin-top="10px">
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="'Description'"/>
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>
    <xsl:apply-templates mode="subsublist"/>
  </fo:block>
</xsl:template>
	
<!--xsl:template match="tei:div" mode="subsublist" priority="-1">
  <xsl:variable name="heading">
    <xsl:call-template name="div-type-as-heading"/>
  </xsl:variable>
  <fo:block margin-left="+20px" margin-top="10px">
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="$heading"/>
      <xsl:with-param name="level" select="3"/>
    </xsl:call-template>
    <fo:block>
      <xsl:apply-templates mode="subsublist"/>
    </fo:block>
  </fo:block>
</xsl:template-->

<xsl:template match="tei:div[not(node())]" mode="subsublist" priority="2"/>
	
<xsl:template match="tei:div" mode="subsublist">
  <xsl:variable name="heading">
    <xsl:call-template name="div-type-as-heading"/>
  </xsl:variable>
  <fo:block margin-left="+20px" margin-top="10px">
    <xsl:call-template name="heading">
      <xsl:with-param name="s" select="$heading"/>
      <xsl:with-param name="level" select="4"/>
    </xsl:call-template>
    <fo:block>
      <xsl:apply-templates/>
    </fo:block>
  </fo:block>
</xsl:template>
	
<xsl:template match="tei:div[@type='ap_structure']" mode="subsublist" priority="2">
  <fo:block margin-left="+20px" margin-top="10px">
    <xsl:variable name="heading">
      <xsl:call-template name="div-type-as-heading"/>
    </xsl:variable>
    <fo:block>
      <xsl:call-template name="heading">
        <xsl:with-param name="s" select="$heading"/>
        <xsl:with-param name="level" select="4"/>
      </xsl:call-template>
      <xsl:apply-templates mode="structure"/>
    </fo:block>
  </fo:block>
</xsl:template>

<xsl:template match="tei:list[@rend='definition']">
  <fo:list-block space-before="5px" provisional-distance-between-starts="200px" provisional-label-separation="5px">
    <xsl:call-template name="display-definition-list"/>
  </fo:list-block>
</xsl:template>

<xsl:template match="tei:list[@rend='definition']" mode="subsublist">
  <fo:list-block space-before="5px" provisional-distance-between-starts="200px" provisional-label-separation="5px">
    <xsl:call-template name="display-definition-list"/>
  </fo:list-block>
</xsl:template>
	
<xsl:template match="tei:list[@rend='ordered']">
  <fo:list-block space-before="5px" margin-left="+20px" provisional-distance-between-starts="30px" provisional-label-separation="5px">
    <xsl:apply-templates mode="numbered-list"/>			
  </fo:list-block>
</xsl:template>
	
<xsl:template match="tei:list[not(@rend)]">
  <fo:list-block space-before="5px" margin-left="+20px" provisional-distance-between-starts="20px" provisional-label-separation="5px">
    <xsl:apply-templates/>
  </fo:list-block>
</xsl:template>
	
<xsl:template match="tei:list[not(@rend)]" mode="subsublist">
  <fo:list-block space-before="5px" margin-left="+20px" provisional-distance-between-starts="20px" provisional-label-separation="5px">
    <xsl:apply-templates/>
  </fo:list-block>
</xsl:template>
	
<xsl:template match="tei:item">
  <fo:list-item>
    <fo:list-item-label end-indent="label-end()"><fo:block>&#x2022;</fo:block></fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:list-item-body>
  </fo:list-item>
</xsl:template>
	
<xsl:template match="tei:item" mode="numbered-list">
  <fo:list-item space-after="5px">
    <fo:list-item-label end-indent="label-end()"><fo:block><xsl:number format="1."/></fo:block></fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:list-item-body>
  </fo:list-item>
</xsl:template>

<xsl:template match="tei:item" mode="inline">
  <xsl:apply-templates/>
</xsl:template>
	
<xsl:template match="tei:label" mode="def-list">
  <fo:list-item>
    <fo:list-item-label end-indent="label-end()"><fo:block><xsl:value-of select="."/></fo:block></fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates select="following-sibling::*[1][self::tei:item]" mode="inline"/>
      </fo:block>
    </fo:list-item-body>    
  </fo:list-item>
</xsl:template>
	
	<xsl:template match="tei:p">
		<fo:block space-before="10px"><fo:inline><xsl:apply-templates/></fo:inline></fo:block>
	</xsl:template>
	
	<xsl:template match="tei:p" mode="subsublist">
		<fo:block space-before="10px"><fo:inline><xsl:apply-templates/></fo:inline></fo:block>
	</xsl:template>
	
	<xsl:template match="tei:ref[not(text())][starts-with(@target, '#')]" priority="3">
      <fo:basic-link color="#1A7979" internal-destination="{substring(@target, 2)}"><xsl:value-of select="substring(@target, 2)"/></fo:basic-link>
	</xsl:template>
	
	<xsl:template match="tei:ref[starts-with(@target, '#')][not(text())]" priority="4">
      <fo:basic-link color="#1A7979" internal-destination="{substring-after(@target, '#')}"><xsl:value-of select="id(substring-after(@target, '#'))/tei:head"/></fo:basic-link>
	</xsl:template>
	
	<xsl:template match="tei:ref[starts-with(@target, '#')]" priority="3">
      <fo:basic-link color="#1A7979" internal-destination="{substring-after(@target, '#')}"><xsl:value-of select="text()"/></fo:basic-link>
	</xsl:template>
	
	<xsl:template match="tei:ref[not(text())]" priority="2">
      <fo:basic-link color="#1A7979" external-destination="{@target}"><xsl:value-of select="@target"/></fo:basic-link>
	</xsl:template>
	
	<xsl:template match="tei:ref">
      <fo:basic-link color="#1A7979" external-destination="{@target}"><xsl:value-of select="text()"/></fo:basic-link>
	</xsl:template>
	
<xsl:template match="tei:note">
  <fo:footnote font-size="smaller">
    <fo:inline baseline-shift="super"><xsl:value-of select="count(preceding::tei:note)+1"/></fo:inline>
    <fo:footnote-body start-indent="0px">
      <fo:block font-size="smaller">
        <fo:inline baseline-shift="super"><xsl:value-of select="count(preceding::tei:note)+1"/></fo:inline>
        <fo:inline space-start="1mm"><xsl:apply-templates/></fo:inline>        
      </fo:block>      
    </fo:footnote-body>
  </fo:footnote>
</xsl:template>
	
	<xsl:template match="tei:figure">
		<fo:block>
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="tei:graphic[@url]">
      <fo:external-graphic src="{@url}" width="100%" content-width="scale-down-to-fit"/>
	</xsl:template>
	
	<xsl:template match="tei:hi[@rend='bold']">
      <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
	</xsl:template>
	
	<xsl:template match="tei:emph">
      <fo:inline font-style="italic"><xsl:apply-templates/></fo:inline>
	</xsl:template>
	 
	<xsl:template match="tei:gi">
      <fo:inline font-family="monospace">&lt;<xsl:value-of select="."/>&gt;</fo:inline>
	</xsl:template>

	<xsl:template match="tei:att">
      <fo:inline font-family="monospace">@<xsl:value-of select="."/></fo:inline>
	</xsl:template>

	<xsl:template match="tei:val">
      <fo:inline font-family="monospace">"<xsl:value-of select="text()"/>"</fo:inline>
	</xsl:template>
	
    	
	<!--file description-->
	
	<xsl:template name="display-eodem-logo">
      <fo:external-graphic src="EODEM 75dpi color transparent background.png" width="100%" content-width="scale-down-to-fit"/>
    </xsl:template>
    
	<xsl:template name="display-lido-logo">
  <fo:block margin-top="200px">
      <fo:float float="right"><fo:block><fo:external-graphic src="CIDOC.jpg" margin-right="0" margin-bottom="0" width="45%" content-width="50%"/></fo:block></fo:float>
  	  <fo:block><fo:external-graphic src="LIDO_logo_main_600x383_trans_dither-w.png" margin-left="0" margin-bottom="0" padding-top="40px" width="45%" content-width="40%"/></fo:block>
  	  </fo:block>
    </xsl:template>
	
	<!--xsl:template match="tei:fileDesc">
      <xsl:apply-templates select="tei:titleStmt|tei:publicationStmt"/>
    </xsl:template-->

<xsl:template match="profile:schemaMeta" mode="front-matter">
      <xsl:apply-templates select="profile:title"/>
      <xsl:apply-templates select="profile:author"/>
      <xsl:apply-templates select="profile:revision/profile:change[1]" mode="version"/>
      <xsl:apply-templates select="profile:date"/>
      <xsl:apply-templates select="profile:licence"/>
      <!--xsl:apply-templates select="profile:revision"/-->
</xsl:template>
	
	<!--xsl:template match="tei:titleStmt">
      <xsl:apply-templates select="tei:title"/>
      <xsl:apply-templates select="tei:author"/>
    </xsl:template-->
	
	<xsl:template match="tei:title|profile:title">
      <xsl:call-template name="heading">
        <xsl:with-param name="s" select="text()"/>
        <xsl:with-param name="level" select="1"/>
        <xsl:with-param name="centred" select="true()"/>
      </xsl:call-template>
	</xsl:template>
	
	<xsl:template match="tei:author|profile:author">
      <xsl:call-template name="heading">
        <xsl:with-param name="s" select="text()"/>
        <xsl:with-param name="level" select="2"/>    
        <xsl:with-param name="centred" select="true()"/>
      </xsl:call-template>
	</xsl:template>

<xsl:template match="profile:change" mode="version">
  <fo:block text-align="center">
    <fo:block margin-top="20px" margin-bottom="20px">
      <xsl:value-of select="concat('Version ', @n)"/>		
    </fo:block>
  </fo:block>
</xsl:template>
	
	<!--xsl:template match="tei:publicationStmt">
      <fo:block text-align="center">
		<xsl:apply-templates/>		
	  </fo:block>
	</xsl:template>
	
<xsl:template match="tei:publicationStmt/tei:date">
  <fo:block margin-top="20px" margin-bottom="20px"><fo:inline font-weight="bold">Publication date <xsl:value-of select="."/></fo:inline></fo:block>
</xsl:template-->

<xsl:template match="profile:date">
    <fo:block text-align="center">
      <fo:block margin-top="20px" margin-bottom="20px"><fo:inline font-weight="bold">Publication date <xsl:value-of select="."/></fo:inline></fo:block>
	  </fo:block>
</xsl:template>

<xsl:template match="profile:licence">
    <fo:block text-align="center">
      <fo:block margin-top="20px" margin-bottom="20px">
        <fo:basic-link external-destination="url({@target})" color="blue" text-decoration="underline"><xsl:value-of select="."/></fo:basic-link></fo:block>
	  </fo:block>
</xsl:template>
	
	<!--revision description-->
	
	<xsl:template match="tei:revisionDesc|profile:revision">
      <xsl:call-template name="heading">
        <xsl:with-param name="s" select="'Version history'"/>
        <xsl:with-param name="level" select="2"/>    
      </xsl:call-template>
      <xsl:apply-templates/>
	</xsl:template>
	
    <xsl:template match="tei:change|profile:change">
      <fo:block margin-left="+10px">
        <xsl:call-template name="heading">
          <xsl:with-param name="s" select="concat(@n, ' ', @when)"/>
          <xsl:with-param name="level" select="3"/>    
        </xsl:call-template>
        <fo:block margin-top="10px" margin-bottom="10px">
          <xsl:apply-templates select="@resp" mode="dl"/>
          <xsl:apply-templates select="tei:note[@type='documentation_status']" mode="dl"/>
          <xsl:apply-templates select="tei:note[@type='documentation_summary']" mode="dl"/>				        
        </fo:block>
        <fo:block><fo:inline font-weight="bold"><xsl:value-of select="'In detail'"/></fo:inline></fo:block>
        <fo:block margin-left="+10px">
          <xsl:apply-templates select="*[not(self::tei:note)]"/>
        </fo:block>			
      </fo:block>
    </xsl:template>
	
	<xsl:template match="@resp" mode="dl">
      <fo:block><fo:inline font-weight="bold"><xsl:value-of select="'Responsible'"/></fo:inline></fo:block>
      <fo:block margin-left="+50px"><fo:inline><xsl:value-of select="."/></fo:inline></fo:block>
	</xsl:template>
	
	<xsl:template match="tei:note" mode="dl">
		<xsl:variable name="heading" select="substring-after(@type, '_')"/>
        <fo:block><fo:inline font-weight="bold"><xsl:value-of select="concat(translate(substring($heading, 1, 1), $lc, $uc), substring($heading, 2))"/></fo:inline></fo:block>
        <fo:block margin-left="+50px"><fo:inline><xsl:apply-templates/></fo:inline></fo:block>
	</xsl:template>
	
	<xsl:template match="*" mode="structure">
		<fo:block margin-left="+10px"><fo:inline>&lt;<xsl:value-of select="name()"/><xsl:apply-templates select="@*" mode="structure"/>&gt;<xsl:apply-templates mode="structure"/></fo:inline></fo:block>
	</xsl:template>
	
	<xsl:template match="@*" mode="structure">
		<xsl:choose>
			<xsl:when test="starts-with(., '#')">
				<xsl:value-of select="concat(' ', name(), '=', $quot)"/>
                <fo:basic-link color="#1A7979" internal-destination="{substring(., 2)}"><xsl:value-of select="substring(., 2)"/></fo:basic-link>
				<xsl:value-of select="$quot"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(' ', name(), '=', $quot, ., $quot)"/>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
<xsl:template match="tei:ref" mode="structure">
  <fo:block margin-left="+10px">
    <fo:basic-link color="#1A7979" internal-destination="{substring(@target, 2)}"><xsl:value-of select="substring(@target, 2)"/></fo:basic-link>
  </fo:block>
</xsl:template>
	
	<xsl:template match="text()" mode="structure">
		<xsl:if test=". != '#'"><fo:inline><xsl:value-of select="."/></fo:inline></xsl:if>
	</xsl:template>

<!-- LIDO profile templates: -->

<xsl:template name="contents-list">
  <fo:block page-break-before="always">
    <xsl:call-template name="heading">
      <xsl:with-param name="level" select="1"/>
      <xsl:with-param name="s" select="'Contents'"/>
    </xsl:call-template>
    <xsl:apply-templates select="//tei:div[@type='guideline_introduction']/tei:div" mode="toc"/>
    <xsl:call-template name="heading">
      <xsl:with-param name="level" select="2"/>
      <xsl:with-param name="s" select="'Elements'"/>
    </xsl:call-template>
    <xsl:apply-templates select="//tei:div[@type='ap_unit'][.//tei:div[@type='ap_structure']/*]" mode="toc">
      <!--xsl:sort select="@xml:id"/-->
    </xsl:apply-templates>
    <xsl:call-template name="heading">
      <xsl:with-param name="level" select="2"/>
      <xsl:with-param name="s" select="'Attributes'"/>
    </xsl:call-template>
    <xsl:apply-templates select="//tei:div[@type='ap_unit'][not(.//tei:div[@type='ap_structure']/*)]" mode="toc">
      <!--xsl:sort select="@xml:id"/-->
    </xsl:apply-templates>
  </fo:block>
</xsl:template>

<xsl:template name="heading">
  <xsl:param name="level" select="3"/>
  <xsl:param name="s" select="text()"/>
  <xsl:param name="centred" select="false()"/>
  <xsl:variable name="size">
    <xsl:choose>
      <xsl:when test="$level=1">24pt</xsl:when>
      <xsl:when test="$level=2">18pt</xsl:when>
      <xsl:when test="$level=3">14pt</xsl:when>
      <xsl:otherwise>12pt</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="weight">
    <xsl:choose>
      <xsl:when test="$level&lt;3">bold</xsl:when>
      <xsl:otherwise>normal</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <fo:block>
    <xsl:attribute name="space-before"><xsl:value-of select="$size"/></xsl:attribute>
    <xsl:attribute name="font-size"><xsl:value-of select="$size"/></xsl:attribute>
    <xsl:attribute name="font-weight"><xsl:value-of select="$weight"/></xsl:attribute>
    <xsl:if test="$centred = true()">
      <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:if>
    <xsl:if test="$level &lt;= 4">
      <xsl:attribute name="color"><xsl:value-of select="'#CA952C'"/></xsl:attribute>
    </xsl:if>
    <xsl:value-of select="$s"/>
  </fo:block>
</xsl:template>
	
<xsl:template name="div-type-as-heading">
		<xsl:choose>
			<xsl:when test="@type='ap_divergence_from_lido'">Divergence from LIDO</xsl:when>
			<xsl:when test="starts-with(@type, 'ap_')">
				<xsl:variable name="head" select="substring-after(@type, 'ap_')"/>
				<xsl:value-of select="translate(substring($head, 1, 1), $lc, $uc)"/>
				<xsl:value-of select="translate(substring($head, 2), '_', ' ')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate(substring(@type, 1, 1), $lc, $uc)"/>
				<xsl:value-of select="translate(substring(@type, 2), '_', ' ')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="display-definition-list">
		<xsl:apply-templates select="tei:label" mode="def-list"/>
	</xsl:template>

<!--Templates borrowed (mostly) from modes-library.xsl:-->
	
	<xsl:template name="pdf-pagesetup">
		<xsl:param name="IsGrid"/>
		<xsl:param name="Margins" select="$pdfopts/Margins"/>
		<xsl:param name="MarginsDoubleSided"
			select="$pdfopts/MarginsDoubleSided"/>
		<xsl:param name="Footers" select="$pdfopts/Footers"/>
		<!--set by default to reportoptions values, can be changed at runtime:-->
		<xsl:param name="PaperSize" select="$pdfopts/PaperSize/text()"/>
		<xsl:param name="Orientation" select="$pdfopts/Orientation/text()"/>
		<xsl:param name="MarginTop" select="$Margins/Top/text()"/>
		<xsl:param name="MarginBottom" select="$Margins/Bottom/text()"/>
		<xsl:param name="MarginLeft" select="$Margins/Left/text()"/>
		<xsl:param name="MarginRight" select="$Margins/Right/text()"/>
		<xsl:param name="DoubleSidedMode"
			select="$MarginsDoubleSided/DoubleSidedMode/text()"/>
		<xsl:param name="MarginInside"
			select="$MarginsDoubleSided/MarginInside/text()"/>
		<xsl:param name="MarginOutside"
			select="$MarginsDoubleSided/MarginOutside/text()"/>
		<!--Provide a quick way of 'turning off' headers/footers at run-time:-->
		<xsl:param name="IncludeHeaderline">
			<xsl:choose>
				<xsl:when test="$headertext">yes</xsl:when>
				<xsl:otherwise>no</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:param name="IncludeFooterline">
			<xsl:choose>
				<xsl:when test="$Footers/*/text()">yes</xsl:when>
				<xsl:otherwise>no</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
        <xsl:variable name="papersizes">
          <paper size="a3" width="297mm" height="420mm"/>
          <paper size="a4" width="210mm" height="297mm"/>
          <paper size="a5" width="148mm" height="210mm"/>
          <paper size="a6" width="105mm" height="148mm"/>
          <paper size="a7" width="74mm" height="105mm"/>
          <paper size="b4" width="250mm" height="353mm"/>
          <paper size="b5" width="176mm" height="250mm"/>
          <paper size="b6" width="125mm" height="176mm"/>
          <paper size="letter" width="8.5in" height="11in"/>
          <paper size="legal" width="8.5in" height="14in"/>
          <paper size="tabloid" width="11in" height="17in"/>
          <paper size="foolscap" width="8in" height="13in"/>
          <paper size="PA4" width="210mm" height="280mm"/>
          <paper size="DL" width="99mm" height="210mm"/>
    <!--You can add in your own paper size definitions here.-->  
  </xsl:variable>
		<!--get page width and height from PaperSize + Orientation:-->
		<xsl:variable name="width">
			<xsl:choose>
				<xsl:when test="$Orientation = 'landscape'">
					<xsl:value-of select="$papersizes/paper[translate(@size,$uc,$lc)=translate($PaperSize,$uc,$lc)]/@height"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$papersizes/paper[translate(@size,$uc,$lc)=translate($PaperSize,$uc,$lc)]/@width"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="height">
			<xsl:choose>
				<xsl:when test="$Orientation = 'landscape'">
					<xsl:value-of select="$papersizes/paper[translate(@size,$uc,$lc)=translate($PaperSize,$uc,$lc)]/@width"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$papersizes/paper[translate(@size,$uc,$lc)=translate($PaperSize,$uc,$lc)]/@height"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--Test for one-sided or two-sided mode:-->
		<xsl:choose>
			<xsl:when test="$DoubleSidedMode = 'yes'">
				<xsl:call-template name="pdf-pagesetup-twosided">
					<xsl:with-param name="width" select="$width"/>
					<xsl:with-param name="height" select="$height"/>
					<xsl:with-param name="margin-top" select="$MarginTop"/>
					<xsl:with-param name="margin-bottom" select="$MarginBottom"/>
					<xsl:with-param name="margin-inside" select="$MarginInside"/>
					<xsl:with-param name="margin-outside" select="$MarginOutside"/>
					<xsl:with-param name="IncludeHeaderline" select="$IncludeHeaderline"/>
					<xsl:with-param name="IncludeFooterline" select="$IncludeFooterline"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="pdf-pagesetup-onesided">
					<xsl:with-param name="width" select="$width"/>
					<xsl:with-param name="height" select="$height"/>
					<xsl:with-param name="margin-top" select="$MarginTop"/>
					<xsl:with-param name="margin-bottom" select="$MarginBottom"/>
					<xsl:with-param name="margin-left" select="$MarginLeft"/>
					<xsl:with-param name="margin-right" select="$MarginRight"/>
					<xsl:with-param name="IncludeHeaderline" select="$IncludeHeaderline"/>
					<xsl:with-param name="IncludeFooterline" select="$IncludeFooterline"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

  <xsl:template name="pdf-pagesetup-onesided">
    <xsl:param name="width"/><xsl:param name="height"/>
    <xsl:param name="margin-top"/><xsl:param name="margin-bottom"/>
    <xsl:param name="margin-left"/><xsl:param name="margin-right"/>
    <xsl:param name="IncludeHeaderline"/><xsl:param name="IncludeFooterline"/>
    <fo:layout-master-set>
      <fo:simple-page-master master-name="prefatory"
        page-width="{$width}" page-height="{$height}" margin="1in">
        <fo:region-body margin-bottom="2cm"/>
        <fo:region-after extent="1cm"/>
      </fo:simple-page-master>
      <fo:simple-page-master master-name="all"
        page-width="{$width}" page-height="{$height}"
        margin-top="{$margin-top}" margin-bottom="{$margin-bottom}"
        margin-left="{$margin-left}" margin-right="{$margin-right}">
        <fo:region-body>
          <xsl:if test="$IncludeHeaderline='yes'">
            <xsl:attribute name="margin-top">1.5cm</xsl:attribute>
          </xsl:if>
          <xsl:if test="$IncludeFooterline='yes'">
            <xsl:attribute name="margin-bottom">1.5cm</xsl:attribute>
          </xsl:if>
        </fo:region-body>
        <xsl:if test="$IncludeHeaderline='yes'">
          <fo:region-before extent="1cm"/>
        </xsl:if>
        <xsl:if test="$IncludeFooterline='yes'">
          <fo:region-after extent="1cm"/>
        </xsl:if>
      </fo:simple-page-master>
    </fo:layout-master-set>
  </xsl:template>

  <xsl:template name="pdf-pagesetup-twosided">
    <xsl:param name="width"/><xsl:param name="height"/>
    <xsl:param name="margin-top"/><xsl:param name="margin-bottom"/>
    <xsl:param name="margin-inside"/><xsl:param name="margin-outside"/>
    <xsl:param name="IncludeHeaderline"/><xsl:param name="IncludeFooterline"/>
    <fo:layout-master-set>
      <fo:simple-page-master master-name="prefatory"
        page-width="{$width}" page-height="{$height}" margin="1in">
        <fo:region-body margin-bottom="2cm"/>
        <fo:region-after extent="1cm"/>
      </fo:simple-page-master>
      <fo:simple-page-master master-name="left"
        page-width="{$width}" page-height="{$height}"
        margin-top="{$margin-top}" margin-bottom="{$margin-bottom}"
        margin-left="{$margin-outside}" margin-right="{$margin-inside}">
        <fo:region-body>
          <xsl:if test="$IncludeHeaderline='yes'">
            <xsl:attribute name="margin-top">1.5cm</xsl:attribute>
          </xsl:if>
          <xsl:if test="$IncludeFooterline='yes'">
            <xsl:attribute name="margin-bottom">1.5cm</xsl:attribute>
          </xsl:if>
        </fo:region-body>
        <xsl:if test="$IncludeHeaderline='yes'">
          <fo:region-before extent="1cm" region-name="xsl-region-before-left"/>
        </xsl:if>
        <xsl:if test="$IncludeFooterline='yes'">
          <fo:region-after extent="1cm" region-name="xsl-region-after-left"/>
        </xsl:if>
      </fo:simple-page-master>
      <fo:simple-page-master master-name="right"
        page-width="{$width}" page-height="{$height}"
        margin-top="{$margin-top}" margin-bottom="{$margin-bottom}"
        margin-left="{$margin-inside}" margin-right="{$margin-outside}">
        <fo:region-body>
          <xsl:if test="$IncludeHeaderline='yes'">
            <xsl:attribute name="margin-top">1.5cm</xsl:attribute>
          </xsl:if>
          <xsl:if test="$IncludeFooterline='yes'">
            <xsl:attribute name="margin-bottom">1.5cm</xsl:attribute>
          </xsl:if>
        </fo:region-body>
        <xsl:if test="$IncludeHeaderline='yes'">
          <fo:region-before extent="1cm" region-name="xsl-region-before-right"/>
        </xsl:if>
        <xsl:if test="$IncludeFooterline='yes'">
          <fo:region-after extent="1cm" region-name="xsl-region-after-right"/>
        </xsl:if>
      </fo:simple-page-master>
      <fo:page-sequence-master master-name="all">
        <fo:repeatable-page-master-alternatives>
          <fo:conditional-page-master-reference master-reference="right"
            odd-or-even="odd"/>
          <fo:conditional-page-master-reference master-reference="left"
            odd-or-even="even"/>
        </fo:repeatable-page-master-alternatives>
      </fo:page-sequence-master>
    </fo:layout-master-set>
  </xsl:template>

  <!--This template generates attributes, so must be called as first child of
the element concerned (usually fo:root).-->
  <xsl:template name="pdf-fontinfo">
    <xsl:param name="IsGrid"/>
    <!--Set in reportoptions by default, can be overridden at run-time:-->
    <xsl:param name="FontFamily">
      <xsl:value-of select="$pdfopts/FontFaces"/>
      <xsl:text/>,<xsl:value-of select="$pdfopts/FontFallback"/>
    </xsl:param>
    <xsl:param name="FontSize" select="$pdfopts/FontSize/text()"/>
    <!--Generate attributes:-->
    <xsl:attribute name="font-family">
      <xsl:value-of select="$FontFamily"/>
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$FontSize"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="pdf-headerfooter">
    <xsl:param name="Left"/><xsl:param name="Right"/><xsl:param name="Centre"/>
    <xsl:choose>
      <xsl:when test="not(concat($Left,$Centre,$Right))"/>
      <xsl:when test="not(string($Centre))">
        <fo:table width="100%">
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell width="50%">
                <fo:block padding-right="5mm" text-align="left">
                  <xsl:call-template name="headerfootertext">
                    <xsl:with-param name="text" select="$Left"/>
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell width="50%">
                <fo:block padding-left="5mm" text-align="right">
                  <xsl:call-template name="headerfootertext">
                    <xsl:with-param name="text" select="$Right"/>
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </xsl:when>
      <xsl:when
        test="not(concat($Left,$Right))">
        <fo:block text-align="center" padding-left="5mm" padding-right="5mm">
          <xsl:call-template name="headerfootertext">
            <xsl:with-param name="text" select="$Centre"/>
          </xsl:call-template>
        </fo:block>
      </xsl:when>
      <xsl:otherwise>
        <fo:table width="100%">
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell width="20%">
                <fo:block padding-right="5mm" text-align="left">
                  <xsl:call-template name="headerfootertext">
                    <xsl:with-param name="text" select="$Left"/>
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell width="40%" padding-right="5mm">
                <fo:block padding-left="5mm" text-align="center">
                  <xsl:call-template name="headerfootertext">
                    <xsl:with-param name="text" select="$Centre"/>
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell width="20%">
                <fo:block padding-left="5mm" text-align="right">
                  <xsl:call-template name="headerfootertext">
                    <xsl:with-param name="text" select="$Right"/>
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="headerfootertext">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="$text='@@@logo@@@'">
        <xsl:variable name="logofile" select="'logo.jpg'"/>
        <xsl:if test="string($logofile)">
          <fo:external-graphic content-height="30px" content-width="scale-to-fit" src="{$logofile}"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="headerfootervariables">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="headerfootervariables">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="contains($text,'@@@')">
        <xsl:value-of select="substring-before($text,'@@@')"/>
        <xsl:variable name="var"
          select="substring-before(substring-after($text,'@@@'),'@@@')"/>
        <xsl:choose>
          <!--xsl:when test="$var = 'filedescription'"><xsl:value-of select="$_filedescription"/></xsl:when>
          <xsl:when test="$var = 'logo'"><xsl:value-of select="$_logo"/></xsl:when-->
          <xsl:when test="$var = 'page'"><fo:page-number/></xsl:when>
          <!--xsl:when test="$var='date'">
            <xsl:call-template name="date-format">
              <xsl:with-param name="date" select="$_date"/>
              <xsl:with-param name="format" select="'%f.%o.%Y'"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$var='time'">
            <xsl:value-of select="$_time"/>
          </xsl:when>
          <xsl:when test="$var='user'">
            <xsl:value-of select="$_user"/>
          </xsl:when>
          <xsl:when test="$var='application'">
            <xsl:value-of select="$_application"/>
          </xsl:when>
          <xsl:when test="$var='filename'">
            <xsl:value-of select="$_filename"/>
          </xsl:when>
          <xsl:when test="$var='indexname'">
            <xsl:value-of select="$_indexname"/>
          </xsl:when>
          <xsl:when test="$var='indexkey'">
            <xsl:value-of select="$_indexkey"/>
          </xsl:when-->
        </xsl:choose>
        <xsl:call-template name="headerfootervariables">
          <xsl:with-param name="text"
            select="substring-after(substring-after($text,'@@@'),'@@@')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="$text"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="pdf-headersection">
    <xsl:param name="IsGrid"/>
    <xsl:param name="DoubleSidedMode"
      select="$pdfopts/MarginsDoubleSided/DoubleSidedMode/text()"/>
    <!--Suppress header if nothing to put in it:-->
    <xsl:if test="$headertext">
      <!--Uses top-level $HeaderLeft etc, see top of file.-->
      <xsl:choose>
        <xsl:when test="$DoubleSidedMode = 'yes'">
          <fo:static-content flow-name="xsl-region-before-right"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$HeaderLeft"/>
              <xsl:with-param name="Right" select="$HeaderRight"/>
              <xsl:with-param name="Centre" select="$HeaderCentre"/>
            </xsl:call-template>
          </fo:static-content>
          <fo:static-content flow-name="xsl-region-before-left"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$HeaderRight"/>
              <xsl:with-param name="Right" select="$HeaderLeft"/>
              <xsl:with-param name="Centre" select="$HeaderCentre"/>
            </xsl:call-template>
          </fo:static-content>
        </xsl:when>
        <xsl:otherwise>
          <fo:static-content flow-name="xsl-region-before"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$HeaderLeft"/>
              <xsl:with-param name="Right" select="$HeaderRight"/>
              <xsl:with-param name="Centre" select="$HeaderCentre"/>
            </xsl:call-template>
          </fo:static-content>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="pdf-footersection">
    <xsl:param name="IsGrid"/>
    <xsl:param name="Footers" select="$pdfopts/Footers"/>
    <!--These normally come from reportoptions control, but could be
overwritten at runtime if required:-->
    <xsl:param name="DoubleSidedMode" select="$pdfopts/MarginsDoubleSided/DoubleSidedMode/text()"/>
    <xsl:param name="FooterRight" select="$Footers/Right/text()"/>
    <xsl:param name="FooterLeft" select="$Footers/Left/text()"/>
    <xsl:param name="FooterCentre" select="$Footers/Centre/text()"/>
    <!--Suppress footer if nothing to put in it:-->
    <xsl:if test="$Footers/*/text()">
      <xsl:choose>
        <xsl:when test="$DoubleSidedMode = 'yes'">
          <fo:static-content flow-name="xsl-region-after-right"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$FooterLeft"/>
              <xsl:with-param name="Right" select="$FooterRight"/>
              <xsl:with-param name="Centre" select="$FooterCentre"/>
            </xsl:call-template>
          </fo:static-content>
          <fo:static-content flow-name="xsl-region-after-left"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$FooterRight"/>
              <xsl:with-param name="Right" select="$FooterLeft"/>
              <xsl:with-param name="Centre" select="$FooterCentre"/>
            </xsl:call-template>
          </fo:static-content>
        </xsl:when>
        <xsl:otherwise>
          <fo:static-content flow-name="xsl-region-after"
            font-size="80%">
            <xsl:call-template name="pdf-headerfooter">
              <xsl:with-param name="Left" select="$FooterLeft"/>
              <xsl:with-param name="Right" select="$FooterRight"/>
              <xsl:with-param name="Centre" select="$FooterCentre"/>
            </xsl:call-template>
          </fo:static-content>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
	
</xsl:stylesheet>