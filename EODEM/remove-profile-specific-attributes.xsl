<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
<xsl:param name="profile-uri" select="'http://www.eodem-schema.org'"/>
<xsl:param name="profile-uri-2"/>
<xsl:param name="schema-location" select="'http://www.lido-schema.org lido-v1.1-profile-eodem-v1.0.xsd'"/>
<xsl:template match="/*" priority="2">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:attribute name="xsi:schemaLocation"><xsl:value-of select="$schema-location"/></xsl:attribute>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>
<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*" priority="-1">
		<xsl:copy/>
	</xsl:template>
	<xsl:template match="@*[namespace-uri()=$profile-uri]"/>
	<xsl:template match="@*[string(namespace-uri()) and namespace-uri()=$profile-uri-2]"/>
</xsl:stylesheet>