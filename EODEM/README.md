# EODEM LIDO profile

These files comprise a single document from which can be generated both machine-readable definitions of an EODEM profile, accompanying human-readable documentation, and some sample documents; along with the stylesheets used to generate them. We have adopted the terminology used by the TEI Consortium, and refer to the single source document as an ODD (One Document Does it all).

The sub-folder /samples_and_tools contains some more samples and generic tools, provided as aids to developers hoping to implement EODEM importers or exporters.

## Folder contents

### The ODD

- `lido-v1.1-profile-EODEM-v1.0.xml` **(D1)**

### XSLT files

Which can be used to generate …:

- Outline EODEM document structure, in HTML: `eodem-document-structure.xsl` **(X1)**
- HTML documentation: `display-profile-as-html.xsl` **(X2)**
- PDF documentation: `display-profile-as-xsl-fo.xsl` **(X3)**
- A sample EODEM XML file:
  - `profile-to-example-record.xsl` **(X4)**
  - `remove-profile-specific-attributes.xsl` **(X5)**

The generic LIDO `../generic/lidodd.xsl` should be used to generate:

- An XSD schema document

### Schematron files

- An XSD schema document which provides Schematron rules referenced by R1: `lido-v1.1-profile-EODEM-v1.0.sch`
- A Schematron XML file which can be used to generate a Schematron validation XSL template: `lido-v1.1-profile-EODEM-v1.0.sch.xml`
- A Schematron XSL template derived from the previous file which can be used to validate EODEM files: `lido-v1.1-profile-EODEM-v1.0.sch.xsl`

### Sample output files

- An EODEM XSD schema document: `lido-v1.1-profile-EODEM-v1.0.xsd` **(R1)**
- An outline of an EODEM document's structure, in HTML: `lido-v1.1-profile-EODEM-v1.0-document-structure.html` **(R2)**
- Documentation of an EODEM document, in HTML: `lido-v1.1-profile-EODEM-v1.0.html` **(R3)**
- Documentation of an EODEM document, in XSL-FO format for transformation to PDF: `lido-v1.1-profile-EODEM-v1.0.fo` **(R4)**
- Documentation of an EODEM document, in PDF: `lido-v1.1-profile-EODEM-v1.0.pdf` **(R5)**
- A sample EODEM XML record: `lido-v1.1-profile-EODEM-v1.0-sample-record.xml` **(R6)**

## A suggested processing workflow

1. process **D1** with the generic LIDO **lidodd.xsl** to produce **R1**
2. process **D1** with **X2** to produce **R3**
3. process **D1** with **X3** to produce **R4**
4. process **R4** with an XSL-FO processor to produce **R5**
5. process **D1** with **X4** to produce `lido-v1.1-profile-EODEM-v1.0-sample-record-with-attributes.xml` **(T1)**, a temporary file - sample record with added profile-specific information
6. process **T1** with **X1** to produce **R2**
7. process **T1** with **X5** to produce **R6**

Sample commands for running these transformations using the Saxon XSL processor are provided in `Saxon_commands.md`.

## Acknowledgements

We are deeply grateful to Richard Light, who has worked through many of the practicalities implied by the ODD technique for defining LIDO profiles, and created the various stylesheets in this folder more-or-less single-handedly.

