# Generic LIDO Application Profile Workflow

This directory contains the files needed to create a LIDO application profile template and to transform it into a
complete XSD.

Based on EODEM workflow.

## How to Use

Specify the desired changes to LIDO 1.1 in a profile definition XML (relevant specifications below). The directory
`generic` contains a `generic_profile_definition_template.xml` that can be modified as needed. The profile definition XML
needs to be in the same directory as `lidodd.xsl`, `html.xsl`, `sch.xsl`, and `style.css` for the process to produce everything
correctly. Apply `lidodd.xsl` to the profile definition XML. The resulting XSD is your application profile XSD, if you
have specified any profile documentation, the process will also produce the documentation in HTML format, as well as a
separate `.sch` file containing only the declared Schematron rules (if present).

## Specification for Profile Definition XML

This section covers the three main areas of the profile definition: metadata, documentation, and rules.
Cf. the profile definition template for additional examples.

### Metadata: `schemaMeta`

This element contains the necessary metadata to identify the profile definition XML as well as the resulting application
profile XSD.

Schema metadata **MUST** contain one of following elements: `title`, `licence`, `URN`. Schema metadata **MUST** contain
at least one `author` element. Each `author` element **MAY** have an attribute `@xml:id` for reference purposes.

The `URN` element contains a sequence of characters identifying the application profile, such as "paintingandsculpture"
or "QA". The text content of this element will be used to automatically create the final URI of the application profile XSD.

Schema metadata **SHOULD** contain a revision history and an abstract. The abstract, contained in the element `abstract`,
should briefly describe the purpose of the application profile. The element `revision` contains the revision history and
contains one or more `change` elements. The `change` elements document the changes between revisions of the profile
definition XML. Each `change` element **MUST** have an attribute `@when` containing an ISO 8601 conforming date string.
Each `change` element **MAY** have the attributes `@who` and `@n`. `@who` identifies the party responsible for the
respective change. `@who` may contain a reference to the `@xml:id` of one of the authors of the profile definition. The
value of the attribute `@n` signifies the version number reached with the documented change, if applicable. The latest
version number is used to automatically create the final URI of the application profile. Because of this, if there is
only one `change` element, it **MUST** have an attribute `@n` conforming to the format `N.N`.

Schema metadata **SHOULD** further contain the profile's date of publication in the element `date`. This **MUST** be a
vais ISO 8601 date.

Schema metadata **MAY** contain one or more `publisher` elements to provide the names of organizations responsible for
the creation of the application profile.

### Documentation: `schemaDoc`

To document your application profile, use the elements of the [TEI Lite specification](https://tei-c.org/guidelines/customization/lite/).
The `generic_profile_definition_template.xml` contains examples for all the use cases of TEI elements that are transformed
into HTML.

### Rules Specification: `schemaSpec`

All LIDO application profiles **MUST** be LIDO 1.1-compatible.

This element contains all of the changes between LIDO 1.1 and the application profile. These may include changes to
element and attribute definitions of LIDO 1.1, written in XSD language. Further, it may contain additional Schematron
rules.

#### Elements and Attribute changes: `elementSpec`

The element `elementSpec` specifies changes to a single element or attribute in the XSD.

Each element `elementSpec` **MUST** have the attributes `@xml:id` and `@mode`. Except for new elements added in the
application profile, `@xml:id` **MUST** correspond to the `@id` of the element in the LIDO 1.1-XSD being modified.
`@mode` is a necessary processing instruction, possible values are: `replace`, `change`, `add`, `delete`.

Take care *not* to introduce any changes that result in breaking compatibility between your application profile and
LIDO 1.1.

#### Schematron Rules: `constraintSpec`

The element `constraintSpec` specifies one or more additional Schematron rule or rules.

Each element `constraintSpec` **MUST** have the attributes `@xml:id` and `@mode`. `@mode` **MUST** be `"add"`.

Each element `constraintSpec` **MUST** contain at least one and **MAY** contain several elements `constraint`, containing
one or several Schematron patterns.

Whether you define one or several `constraintSpec`, whether these contain one or more `constraint` children, and whether
these contain one or more Schematron patterns is up to you. Choose a structure that keeps the internal logic and the
readability of the profile definition in mind.

## Additional Resources

In creating application profiles, creating a tabular overview of the profile's
conception has proven helpful. One example is [this table (in German)](https://doi.org/10.11588/data/CHEPS6)
giving an overview of the application profile "Painting and Sculpture". It lists all LIDO elements and their attributes
hierarchically, shows an element's cardinality, and lists any additional constraints such as terminology recommendations
that might be checked via Schematron rules. This repository includes an empty template that can be used as a starting
point to create conceptual overviews of application profiles: `profile_design_template.xsl`.
