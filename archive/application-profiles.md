# Readme for LIDO Application Profiles
This document provides guidelines for the creation and editing of LIDO Application Profiles by the LIDO working group *LIDO AG*. The document explains the key structure and component of a LIDO Application Profile and walks through the process of developing a profile. The document is aimed at designers of application profiles.

## Usage
All application profiles are in the `applications-profiles/` folder. Each application profile has its own folder. Please keep the given structure to create or update an application profile:
- `applications-profiles/name-of-your-application-profile/`
- `applications-profiles/name-of-your-application-profile/schema-file.xsd`
- `applications-profiles/name-of-your-application-profile/img/`
- `applications-profiles/name-of-your-application-profile/accompanying-texts/`
- `1.1/accompanying-texts/`

### Schema file
The XML Schema Definition file (.xsd) is the main component of an application profile. It specifies how to formally describe the elements and how to use them (documentation) in the scope of the application. The following applies: The entire scheme is always LIDO valid. Typically, the focus is on documentation in order to support users when using a profile. Please visit [**application-profiles-documentation.md**](../docs/application-profiles-schemafile.md) to learn how to create and update such an documentation.
A manual will be automatically generated from the schema file. It is important that the given structure and format in the schema file are adhered to. For more information about this automated generation visit [**lido-publication**](https://gitlab.gwdg.de/lido/lido-publication).

### Image folder
To use images in the documentation read [**application-profiles-documentation.md**](../docs/application-profiles-documentation.md). All images used must be in this folder.

### Accompanying texts
The schema allows the use of accompanying texts so that shared or more often used texts can be outsourced. This increases the overview and avoids duplication of work. Please create one file for each accompanying texts and visit [**application-profiles-accompanying-texts.md**](../docs/application-profiles-accompanying-texts.md) to learn how to use them.
