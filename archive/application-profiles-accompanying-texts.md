# Readme for LIDO Application Profiles - Accompanying texts
This document provides guidelines for the creation and editing of accompanying texts for LIDO Application Profiles by the LIDO working group *LIDO AG*. The application profile schema allows the use of accompanying texts so that shared or more often used texts can be outsourced. This increases the overview and avoids duplication of work. There are generally two types of accompanying texts:
- general accompanying texts of LIDO in `1.1/accompanying-texts/`
- application profile specific accompanying texts in `applications-profiles/name-of-your-application-profile/accompanying-texts/`

## Usage
To add a new accompanying text, you need to create a new *.xml file in one of the above mentioned folders. Please start with the following frame:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml"
	schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:lido="http://www.lido-schema.org">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Title</title>
      </titleStmt>
      <publicationStmt>
        <p>Information about publication or distribution</p>
      </publicationStmt>
      <sourceDesc>
        <p>Information about the source</p>
      </sourceDesc>
    </fileDesc>
  </teiHeader>
  <text>
    <body>
      <div xml:id="Enter-Your-ID-Here">
        <div xml:lang="Enter-Your-Language-Here">
          <head n="1">Title</head>
          <p>
          YOUR TEXT HERE
          </p>
        </div>
      </div>
    </body>
  </text>
</TEI>
```

To integrate the file and thus the accompanying text into a profile, proceed as follows:
1. Choose a unique ID and replace "Enter-Your-ID-Here"
2. Write your desired accompanying text with all options available [**see application-profiles-documentation.md**](../docs/application-profiles-documentation.md) and replace "YOUR TEXT HERE"
3. Integrate the file and the unique ID in the schema file (.xsd) where you want it as follows:

```xml
<xs:annotation xml:id="accompanying-texts">
  <xs:documentation>
    <xi:include href="accompanying-texts/YOUR-FILE-NAME.xml" xpointer="Your-ID-Here"/>
  </xs:documentation>
</xs:annotation>
```
The automated process of publication will include the accompanying texts in the appropriate language of the documentation.
